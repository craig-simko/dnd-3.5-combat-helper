<?php

//include_once('libraries/combat/CombatHelper.php');
include_once('libraries/combatant/Damage.php');
include_once('libraries/combatant/Attack.php');
include_once('libraries/combatant/AttackMode.php');
include_once('libraries/combatant/Combatant.php');
include_once('libraries/combatant/Save.php');
include_once('libraries/combatant/ArmorClass.php');
include_once('libraries/combatant/CombatantFactory.php');

include_once('libraries/encounter/Encounter.php');
include_once('libraries/encounter/EncounterFactory.php');

include_once('libraries/area/Area.php');
include_once('libraries/area/AreaFactory.php');

include_once('libraries/combat/Combat.php');

?>