<?php

ini_set('xdebug.var_display_max_depth', -1);
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_data', -1);

include_once('config.php');

include_once('inc.php');

// Make sure to invoke session start after including classes
session_start();

$areaNamesNavBar = AreaFactory::GetAreaList();

$selectedArea = isset($_GET['area']) ? $_GET['area'] : '';

if($selectedArea != ''){
    $_SESSION['activeArea'] = AreaFactory::CreateAreaByName($_GET['area']);

    $selectedEncounter = isset($_GET['encounter']) ? $_GET['encounter'] : '';

    $encounterNamesNavBar = $_SESSION['activeArea']->GetEncounterList();
    
    if($selectedEncounter != ''){
        $_SESSION['activeEncounter'] = EncounterFactory::CreateEncounterByName($_GET['encounter']);
        
        if($_SESSION['activeEncounter']->CombatantCount() == 0){
            unset($_SESSION['activeCombat']);
        }else{
            $_SESSION['activeCombat'] = new Combat($_SESSION['activeEncounter']->Combatants());
        }
        
    }else{
        unset($_SESSION['activeCombat']);
    }
}

include_once('views/header.php');

if(!isset($_GET['a'])){
    $_GET['a'] = '';
}

switch($_GET['a']){
    case('create-combatant'):
        include_once('views/combatant_creator/combatant_creator.php');
    break;
    
    case('view-combatants'):
        include_once('views/combatant_view/combatants_view.php');
    break;
    
    case('view-combatant'):
        include_once('views/combatant_view/combatant_view.php');
    break;

    case('create-encounter'):
        include_once('views/encounter_creator/encounter_creator.php');
    break;

    case('create-area'):
        include_once('views/area_creator/area_creator.php');
    break;

    case('adventure'):

        if( isset($_GET['area']) ){                        
            include_once('views/adventure/hub.php'); 
        }else{
            include_once('views/home.php');
        }

    break;

    default:
        include_once('views/home.php');
    break;
}

include_once('views/footer.php');

?>