<?php


class Attack{
    
    private $range;
    private $type;
    private $baseAttackBonus;
    private $attackAttrs;
    private $attackBonus;
    private $damages;
    private $critRangeLow;
    
    /**
    * Instantiates an attack
    * 
    * @param    int    range              Range increment multiple of 5. 5ft, 10ft, 100ft, etc
    * @param    str    type               RANGED|MELEE
    * @param    int    baseAttackBonus    BAB
    * @param    int    attackBonus        How much to add to the d20 roll
    * @param    array  damages            Array of Class Damage
    * @param    int    critRangeLow       Low end crit range, such as 18, 19, 20
    */  
    public function __construct($range, $type, $baseAttackBonus, $attackAttrs, $attackBonus, $damages, $critRangeLow) {

        $this->range           = $range;
        $this->type            = $type;
        $this->baseAttackBonus = $baseAttackBonus;
        $this->attackBonus     = $attackBonus;
        $this->attackAttrs     = $attackAttrs;
        $this->damages         = $damages;
        $this->critRangeLow    = $critRangeLow;

    }
    
    /**
     * Make an attack!
     * 
     * @param    obj    $combatant                  Combatant class object
     * @param    int    $extraAttackBonus           Attack bonus above and beyond normal. High ground, flanking, etc.
     * @param    int    $targetAc                   What number do we need to meet or beat to score a hit?
     * @param    int    $naturalAttackRoll          DEFAULT NULL. If we want to send what we rolled manually.
     * @param    array  $damageReductionTypes       Array of Damage types that the target has DR against
     * @param    int    $damageReductionAmount      Amount of DR the target has.
     * @param    int    $naturalConfirmationRoll    DEFAULT NULL. If we want to send what we rolled for a crit conf manually.
     * 
     * @retun    array  ( 'natural_attack_roll'              => int 
                          'adjusted_attack_roll'             => int,
                          'natural_confirmation_attack_roll' => int,
                          'adjusted_confirmation_roll'       => int,
                          'hit'                              => bool,
                          'crit'                             => bool,
                          'damage'                           => int )
     */
    public function CalculateAttack($combatant, $extraAttackBonus, $targetAc, $damageReductionTypes = array(), $damageReductionAmount = 0, $naturalAttackRoll = null, $naturalConfirmationRoll = null){
        
        if(is_null($naturalAttackRoll)){
            $naturalAttackRoll = rand(1, 20);    
        }
        
        $crit   = false;
        $hit    = false;
        $damage = array();
        
        if($naturalAttackRoll == 1){
            return array( 'natural_attack_roll'              => $naturalAttackRoll, 
                          'adjusted_attack_roll'             => 0,
                          'natural_confirmation_attack_roll' => 0, 
                          'adjusted_confirmation_roll'       => 0,
                          'hit'                              => false,
                          'crit'                             => $crit,
                          'damage'                           => $damage,
                          'damage_string'                    => $this->GetDamageString($crit, $damageReductionTypes, $damageReductionAmount) );
        }
        
        $adjustedConfirmationRoll = 0;
        
        //$attrMod = $combatant->AttributeMod($this->attackAttr);
        
        $attrMod = 0;
        
        foreach($this->attackAttrs as $attr){
            $attrMod += $combatant->AttributeMod($attr);
        }
        
        if($naturalAttackRoll >= $this->critRangeLow){
            
            if(is_null($naturalConfirmationRoll)){
                $naturalConfirmationRoll = rand(1, 20);                
            }
            
            $adjustedConfirmationRoll = $naturalConfirmationRoll + $attrMod + $this->baseAttackBonus + $this->attackBonus + $extraAttackBonus + $combatant->SizeModifier();
            
            if($adjustedConfirmationRoll > $targetAc){
                $crit = true;
            }
        }
        
        $attackRoll = $naturalAttackRoll + $attrMod + $this->baseAttackBonus + $this->attackBonus + $extraAttackBonus + $combatant->SizeModifier();
        
        if($attackRoll >= $targetAc || $naturalAttackRoll == 20){
            
            foreach($this->damages as $d){
                $damage[] = $d->CalculateDamage($attrMod, $crit, $damageReductionTypes, $damageReductionAmount);
            }
            
            return array( 'natural_attack_roll'              => $naturalAttackRoll, 
                          'adjusted_attack_roll'             => $attackRoll,
                          'natural_confirmation_attack_roll' => $naturalConfirmationRoll, 
                          'adjusted_confirmation_roll'       => $adjustedConfirmationRoll,
                          'hit'                              => true,
                          'crit'                             => $crit,
                          'damage'                           => $damage,
                          'damage_string'                    => $this->GetDamageString($crit, $damageReductionTypes, $damageReductionAmount) );
        }
        
        return array( 'natural_attack_roll'              => $naturalAttackRoll, 
                      'adjusted_attack_roll'             => $attackRoll,
                      'natural_confirmation_attack_roll' => $naturalConfirmationRoll, 
                      'adjusted_confirmation_roll'       => $adjustedConfirmationRoll,
                      'hit'                              => false,
                      'crit'                             => $crit,
                      'damage'                           => $damage,
                      'damage_string'                    => $this->GetDamageString($crit, $damageReductionTypes, $damageReductionAmount) );
        
    }
    
    /**
     * Returns the full string of all the damages in the attack.
     *
     * @param    bool   $crit                       Was it a crit? 
     * @param    array  $damageReductionTypes       Array of Damage types that the target has DR against
     * @param    int    $damageReductionAmount      Amount of DR the target has.
     * 
     * @return   string   2d6+12 etc.
     */
    public function GetDamageString($combatant, $crit, $damageReductionTypes = array(), $damageReductionAmount = 0){
        
        $damageStrings = array();
        
        foreach($this->damages as $d){
            $damageStrings[] = $d->GetDamageString($combatant, $crit);
        }
        
        $damageReductionAmount = $this->GetDamageReductionAmount($damageReductionTypes, $damageReductionAmount);
        
        
        
        return implode(' + ', $damageStrings);        
    }
    
    public function GetCritString(){
        $critRange = (intval($this->critRangeLow) != 20 ? $this->critRangeLow . '-20' : '');
        
        $critMultiplier = 0;
        
        foreach($this->damages as $d){
            $multiplierToTest = $d->CritMultiplier();
            
            if($multiplierToTest > $critMultiplier){
                $critMultiplier = $multiplierToTest;
            }
        }
        
        if($critMultiplier != 1 && $critMultiplier != 2 && $critMultiplier != 0){
            $critMultiplier = 'x' . $critMultiplier;
        }else{
            $critMultiplier = '';
        }        
        
        if($critMultiplier != '' || $critRange != ''){
            return '/' . $critRange . ($critMultiplier != '' ? 'x' : '') . $critMultiplier;
        }else{
            return '';
        }
    }
    
    /**
      * Returns the amount of damage reduced by damage reduction for a specific target.
      * 
      * @param     array     damageReductionTypes
      * @param     int       amount of damage resistance
      * 
      * @return    int       Amount of damage resisted
      */
     public function GetDamageReductionAmount($damageReductionTypes, $damageReductionAmount){
        $reductionTypesFound = 0;
        
        $damageTypes = array();
        
        foreach($this->damages as $d){
            $types = $d->GetDamageTypes();
            
            $damageTypes = array_merge($types, $damageTypes);
        }
        
        foreach($damageTypes as $type){
            if(in_array($type, $damageReductionTypes)){
                $reductionTypesFound++;
            }
        }
        
        if($reductionTypesFound == count($damageReductionTypes)){
            $damageReductionAmount = 0;
        }
        
        return $damageReductionAmount;
     }
     
     /**
      * Returns the attack bonus
      * 
      * @return    int    the attack bonus
      */
     public function AttackBonus(){
        return $this->attackBonus;
     }
     
     /**
      * Returns the BAB of this specific attack
      * 
      * @return    int    the bab of this attack
      */
     public function BaseAttackBonus(){
        return $this->baseAttackBonus;
     }
     
     /**
      * Returns the Attr Modifier bonus for this specific attack
      * 
      * @param     obj     combatant object
      * 
      * @return    int     total mod bonus for this attack
      */
     public function ModBonus($combatant){
        $total = 0;
        
        foreach($this->attackAttrs as $attr){
            $total += $combatant->AttributeMod($attr);
        }
        
        return $total;
     }
     
     /**
      * Returns the type of the attack
      * 
      * @return    string    The type: MELEE|RANGED|MELEE_TOUCH|RANGED_TOUCH
      */
     public function AttackType(){
        return $this->type;
     }
     
}

?>