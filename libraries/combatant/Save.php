<?php


class Save{
    
    private $type         = '';
    private $base         = 0;
    private $attributes   = array();
    
    /**
    * Instantiates an attack
    * 
    * @param    str    type            REF | FORT | WILL
    * @param    int    base            Base Save of the throw
    * @param    array  attributes      What attribute(s) affect the roll?
    */  
    public function __construct($type, $base, $attributes) {
    
        $this->type       = $type;
        $this->base       = $base;
        $this->attributes = $attributes;
    
    }
    
    /**
     * Make a save!
     * 
     * @param     Obj     Object of the combatant class
     * @param     int     Target DC 
     * @param     int     Extra Bonuses to the roll above and beyond what the combatant has to offer
     * @param     int     DEFAULT NULL. If not null then use that roll. This is in order to allow real life rolling
     * @param     bool    Auto fail on natural one? Almost always true unless Impetuous Endurance or something of the sort is in play
     */
    public function MakeSavingThrow($combatant, $targetDc, $extraBonuses = 0, $naturalRoll = null, $autoFailOnNaturalOne = true){
        
        if(is_null($naturalRoll)){
            $naturalRoll = rand(1, 20);    
        }
                
        if($naturalRoll == 1 && $autoFailOnNaturalOne){
            return array( 'natural_save_roll'  => $naturalRoll, 
                          'adjusted_save_roll' => 0,
                          'made'               => false );
        }
        
        $adjustedRoll = $naturalRoll;
        
        foreach($this->attributes as $attr){            
            $adjustedRoll += $combatant->AttributeMod($attr);    
        }
        
        $adjustedRoll += $extraBonuses;
        
        $adjustedRoll += $this->base;
        
        $made = $adjustedRoll >= $targetDc;        
        
        return array( 'natural_save_roll'  => $naturalRoll, 
                      'adjusted_save_roll' => $adjustedRoll,
                      'made'               => $made );
    }
    
    /**
     * Returns detailed array of saves and bonuses
     * 
     * @param    obj    combatant     A combatant object.
     * 
     * @return   array 
     */
    public function GetSaveBreakDown($combatant){
        
        $breakDown = array();
        
        $mod = 0;
        
        foreach($this->attributes as $attr){            
            $attrMod = $combatant->AttributeMod($attr);
            
            $mod += $attrMod;
            
            $breakDown['attrs'][] = array('attr' => $attr, 'bonus' => $attrMod);    
        }
        
        $breakDown['base'] = $this->base;
        $breakDown['type'] = $this->type;
        
        $total = $mod + $this->base;
        
        $breakDown['total_bonus'] = $total;
        
        return $breakDown;
    }
}

?>