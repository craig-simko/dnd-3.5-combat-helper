<?php

class AttackMode{
    
    private $title;
    private $attacks;
    private $attacksMadeThisRound = 0;
    
    /**
     * Creates an AttackMode
     * 
     * @param    str     title     The name of the attack mode. Standard Attack, Full Attack, Power Attack, etc.
     * @param    arraay  attacks   An array of all the Attack objects that make up this mode.
     */
    public function __construct($title, $attacks){
       
        $this->title   = $title; 
        $this->attacks = $attacks;
        
    }
    
    /**
     * Makes an attack!
     * 
     * @param     int     $targetAc                    What number do we need to meet or beat to score a hit? 
     * @param     int     $extraBonuses                Can be +2 for flank, -2 for cover etc.
     * @param     array   damageReductionTypes         An array of types that the hit entity has DR for
     * @param     int     damageReductionAmount        Amount of damage that will reduce the damamge amount by if the attack doesn't get through DR
     * @param     int     $naturalAttackRoll           DEFAULT NULL, If we want to roll IRL
     * @param     int     $naturalConfirmationRoll     DEFAULT NULL, if we want to roll IRL
     *
     * @return    array   An array containing info about the attack.
     */
    public function MakeAttack($targetAc, $extraBonuses = 0, $damageReductionTypes = array(), $damageReductionAmount = 0, $naturalAttackRoll = null, $naturalConfirmationRoll = null){
        
        // we already made all the attacks we could!
        if(count($this->attacks) == $this->attacksMadeThisRound){
            return false;
        }
        
        $attackResult = $this->attacks[$this->attacksMadeThisRound]->CalculateAttack($extraBonuses, $targetAc, $damageReductionTypes, $damageReductionAmount, $naturalAttackRoll, $naturalConfirmationRoll);
                
        $this->attacksMadeThisRound++;
        
        return $attackResult;
    }
    
    /**
     * Returns the number of attacks left in this attack mode.
     * 
     * @return     int     Number of attacks left to make in this mode this round.
     */
    public function NumberOfAttacksLeft(){
        return count($this->attacks) - $this->attacksMadeThisRound;
    }
    
    /**
     * Returns the name of the attack mode.
     * 
     * @return     str     name of the attack mode.
     */
    public function Title(){
        return $this->title;
    }
    
    /**
     * Get highest BAB
     * 
     * @return    int    highest BAB of all attacks in this mode
     */
    public function HighestBab(){

        $highestBab = 0;
        
        foreach($this->attacks as $attack){
            
            $testBab = intval($attack->BaseAttackBonus());
            
            if($testBab > $highestBab){
                $highestBab = $testBab;
            }
        }
        
        return $highestBab;
    }

    /**
     * Returns the full attack string block
     * 
     * @param     obj     combatant object that is makign the attack
     * 
     * @return    str     Such as: +11/+6/+1  2d8+9
     */
    public function AttackString($combatant){

        $ret = '';

        $lastDamageString     = '';
        $lastMultiplierString = '';

        $first    = true;
        $firstMod = true;

        foreach($this->attacks as $attack){
            $mod = $attack->AttackBonus() + $attack->BaseAttackBonus() + $attack->ModBonus($combatant) + $combatant->SizeModifier();

            $damageString = $attack->GetDamageString($combatant, false);

            if(!$first && $damageString != $lastDamageString){
                $ret .= ' (' . $lastDamageString . '), ';
                $firstMod = true;
            }
            
            $mod = ($firstMod ? '' : '/') . ($mod >= 0 ? '+' : '') . $mod;
            
            $ret .= $mod;
            
            $first    = false;
            $firstMod = false;
            
            $lastDamageString = $damageString;
        }
        
        $ret .= ' (' . $damageString . $this->attacks[0]->GetCritString() . ')';
        
        return $ret;
    }
    
    /**
     * Resets attack counter
     */
    public function ResetAttackCounter(){
        $this->attacksMadeThisRound = 0;
    }
    
    public function AttackType(){
        return $this->attacks[0]->AttackType();
    }
}

?>