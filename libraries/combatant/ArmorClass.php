<?php


class ArmorClass{
    
    private $armorBonus        = 0;
    private $shieldBonus       = 0;
    private $dodgeBonus        = 0;
    private $deflectionBonus   = 0;
    private $naturalArmorBonus = 0;
    private $maxDexBonus       = 0;
    private $attributes        = array('DEX');
    
    /**
    * Instantiates Armor Class
    * 
    * @param    int    Armor Bonus
    * @param    int    Shield Bonus
    * @param    int    Size Modifier
    * @param    int    Dodge Bonus
    * @param    int    Deflection Bonus
    * @param    int    Natural Armor Bonus
    * @param    int    Max Dex
    * @param    array  An array of Attributes that adjust Armor class. Default: DEX
    */  
    public function __construct($armorBonus, $shieldBonus, $sizeModifier, $dodgeBonus, $deflectionBonus, $naturalArmorBonus, $maxDex, $attributes = array('DEX')) {
    
        $this->armorBonus        = $armorBonus;
        $this->shieldBonus       = $shieldBonus;
        $this->sizeModifier      = $sizeModifier;
        $this->dodgeBonus        = $dodgeBonus;
        $this->deflectionBonus   = $deflectionBonus;
        $this->naturalArmorBonus = $naturalArmorBonus;
        $this->maxDexBonus       = $maxDex;
        
        if(is_array($attributes)){
            $this->attributes    = $attributes;    
        }else{
            $this->attributes    = array($attributes);
        }
        
    
    }
    
    /**
     * Armor Class
     * 
     * @param    obj   the combatant that is being attacked
     * 
     * @return   int   Armor class
     */
    public function ArmorClass($combatant){
        
        $sizeModifier = $combatant->SizeModifier();
        
        $ac   = 10 + 
                $this->armorBonus + 
                $this->shieldBonus + 
                $sizeModifier + 
                $this->deflectionBonus + 
                $this->naturalArmorBonus;
        
        $ac += $this->dodgeBonus;
        
        foreach($this->attributes as $attr){
            if($attr == 'DEX'){
                $ac += max( $this->maxDexBonus, $combatant->AttributeMod($attr) );    
            }else{
                $ac += $combatant->AttributeMod($attr);
            }                
        }
               
        return $ac;
    }

    /** Flat Footed Armor Class
     * 
     * @param     obj    the combatant that is being attacked
     * 
     * @return    int    flat footed ac
     */
    public function FlatFootedArmorClass($combatant){
        $sizeModifier = $combatant->SizeModifier();

        $ac = 10 +
              $sizeModifier +
              $this->armorBonus +
              $this->shieldBonus +
              $this->deflectionBonus +
              $this->naturalArmorBonus;

        return $ac;
    }

    /**
     * Touch Armor Class
     * 
     * @param    obj   the combatant that is being attacked
     * 
     * @return   int   Touch AC
     */
    public function TouchArmorClass($combatant){
        
        $sizeModifier = $combatant->SizeModifier();
        
        $ac   = 10 + 
                $sizeModifier + 
                $this->deflectionBonus;
        
        if(!$combatant->IsFlatFooted()){
            
            $ac += $this->dodgeBonus;
            
            foreach($this->attributes as $attr){
                if($attr == 'DEX'){
                    $ac += max( $this->maxDexBonus, $combatant->AttributeMod($attr) );    
                }else{
                    $ac += $combatant->AttributeMod($attr);
                }                
            }
        }
               
        return $ac;
    }
}

?>