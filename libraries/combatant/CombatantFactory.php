<?php

class CombatantFactory{
           
    /**
     * This function takes all the data necessary for a combatant and serializes it into a json string.
     */     
    public static function SerializeCombatantData(  $name, 
                                                    $flavorText,
                                                    $baseHitPoints, 
                                                    $sizeCategory, 
                                                    $alignmentLawVsChaos,
                                                    $alignmentGoodVsEvil, 
                                                    $landSpeed,
                                                    $swimSpeed,
                                                    $flySpeed,
                                                    $burrowSpeed,
                                                    $climbSpeed,
                                                    $skills,
                                                    $creatureTypes,
                                                    $creatureSubTypes,
                                                    $hitDice,
                                                    $attributes, 
                                                    $saves, 
                                                    $attackModes,
                                                    $armorClassBonuses, 
                                                    $tactics, 
                                                    $morale, 
                                                    $combatGear,
                                                    $specialAbilities, 
                                                    $hasCombatReflexes = false,
                                                    $initiativeBonuses = 0, 
                                                    $languages = array(),
                                                    $damageReduction = array(),
                                                    $damageResistance = array(),
                                                    $initiativeAttribute = 'DEX', 
                                                    $temporaryHp = 0,
                                                    $descriptor = '' ){

        $combatantData = array(
        
            'name'                 => $name,
            'flavorText'           => $flavorText,
            'baseHitPoints'        => $baseHitPoints,
            'sizeCategory'         => $sizeCategory,
            'alignmentLawVsChaos'  => $alignmentLawVsChaos,
            'alignmentGoodVsEvil'  => $alignmentGoodVsEvil, 
            'landSpeed'            => $landSpeed,
            'swimSpeed'            => $swimSpeed,
            'flySpeed'             => $flySpeed,
            'burrowSpeed'          => $burrowSpeed,
            'climbSpeed'           => $climbSpeed,
            'skills'               => $skills,
            'creatureTypes'        => $creatureTypes,
            'creatureSubTypes'     => $creatureSubTypes,
            'hitDice'              => $hitDice,
            'attributes'           => $attributes,
            'saves'                => $saves,
            'attackModes'          => $attackModes,
            'armorClassBonuses'    => $armorClassBonuses,
            'tactics'              => $tactics,
            'morale'               => $morale,
            'combatGear'           => $combatGear,
            'specialAbilities'     => $specialAbilities,
            'hasCombatReflexes'    => $hasCombatReflexes,
            'initiativeBonus'      => $initiativeBonuses,
            'languages'            => $languages,            
            'damageReduction'      => $damageReduction,
            'damageResistance'     => $damageResistance,
            'initiativeAttribute'  => $initiativeAttribute, 
            'temporaryHp'          => $temporaryHp,
            'descriptor'           => $descriptor
        
        );
        
       $encodedData = json_encode($combatantData);
              
       file_put_contents('combatants' . DIRECTORY_SEPARATOR . $name . '.json', $encodedData);
        
    }
    
    /**
     * Returns a combatant object from its name
     * 
     * @param    string      combatantName    The name of the combatant file WITHOUT the .json extension
     * @param    int|null    renameCount      If we want to rename a combatant OBJECT then send the count here.
     * 
     * @return   obj       combatant
     */
    public static function CreateCombatantByName( $combatantName, $renameCount = null ){
        
        $data = file_get_contents(ROOT . DIRECTORY_SEPARATOR . 'combatants' . DIRECTORY_SEPARATOR . $combatantName . '.json');
        
        $combatant = CombatantFactory::DeserializeCombatantData( $data );
        
        if(!is_null($renameCount)){
            $combatant->RenameByCount($renameCount);
        }
        
        return $combatant;        
    }
    
    /**
     * Returns a PC combatant object from its name
     * 
     * @param    string      combatantName    The name of the combatant file WITHOUT the .json extension
     * 
     * @return   obj       combatant
     */
    public static function CreatePcCombatantByName( $pcName ){
        $data = file_get_contents(ROOT . DIRECTORY_SEPARATOR . 'pcs' . DIRECTORY_SEPARATOR . $pcName . '.json');
        
        $combatant = CombatantFactory::DeserializeCombatantData( $data );
                
        return $combatant; 
    }
    
    /**
     * Returns a Combatant Object from an array of data
     * 
     * @param    array|string    An array from the json_decoded string created by SerializeCombatantData. OR json_string created by SerializeCombatantData
     * 
     * @return   obj      Combatant()
     */
    public static function DeserializeCombatantData( $data ){
        
        if(!is_array($data)){
            $data = json_decode($data, true);
        }
        
        $name                   = $data['name'];
        $flavorText             = $data['flavorText'];
        $baseHitPoints          = $data['baseHitPoints'];
        $sizeCategory           = $data['sizeCategory'];
        $alignmentLawVsChaos    = $data['alignmentLawVsChaos'];
        $alignmentGoodVsEvil    = $data['alignmentGoodVsEvil'];
        $landSpeed              = $data['landSpeed'];
        $swimSpeed              = $data['swimSpeed'];
        $flySpeed               = $data['flySpeed'];
        $burrowSpeed            = $data['burrowSpeed'];
        $climbSpeed             = isset($data['climbSpeed']) ? $data['climbSpeed'] : '';
        $skills                 = $data['skills'];
        $creatureTypes          = $data['creatureTypes'];
        $creatureSubTypes       = $data['creatureSubTypes'];
        $hitDice                = $data['hitDice'];
        $attributes             = $data['attributes'];
        $saves                  = $data['saves'];
        $attackModes            = $data['attackModes'];
        $armorClassBonuses      = $data['armorClassBonuses'];
        $tactics                = $data['tactics'];
        $morale                 = $data['morale'];
        $combatGear             = isset($data['combatGear']) ? $data['combatGear'] : '';
        $hasCombatReflexes      = $data['hasCombatReflexes'];
        $initiativeBonus        = $data['initiativeBonus'];
        $languages              = $data['languages'];
        $specialAbilities       = $data['specialAbilities'];
        $damageReduction        = $data['damageReduction'];
        $damageResistance       = $data['damageResistance'];
        $initiativeAttribute    = $data['initiativeAttribute'];
        $temporaryHp            = $data['temporaryHp'];
        $descriptor             = isset($data['descriptor']) ? $data['descriptor'] : '';

        // Create saving throw objects
        $savedObjects = array();
        
        foreach($saves as $save){
            $saveObjects[]      = CombatantFactory::DeserializeSave($save);    
        }        
        // Create attack mode objects 
        $attackModeObjects      = CombatantFactory::DeserializeAttackModes($attackModes);
        
        // Create armor class object
        $armorClass             = CombatantFactory::DeserializeArmorClass($armorClassBonuses);
            
        $combatant = New Combatant( $name, 
                                    $flavorText,
                                    $baseHitPoints, 
                                    $sizeCategory,
                                    $alignmentLawVsChaos,
                                    $alignmentGoodVsEvil,
                                    $landSpeed,
                                    $swimSpeed,
                                    $flySpeed,
                                    $burrowSpeed,
                                    $climbSpeed,
                                    $skills,
                                    $creatureTypes,
                                    $creatureSubTypes,
                                    $hitDice,
                                    $attributes,
                                    $saveObjects, 
                                    $attackModeObjects, 
                                    $armorClass, 
                                    $tactics,
                                    $morale,
                                    $combatGear,
                                    $hasCombatReflexes,
                                    $initiativeBonus,
                                    $languages,
                                    $specialAbilities,
                                    $damageReduction,
                                    $damageResistance,
                                    $initiativeAttribute,
                                    $temporaryHp,
                                    $descriptor              );
                                    
        return $combatant;
    }
    
    /**
     * Returns an array of attack mode objects
     *
     * @param     array
     *
     * @return    array of attack mode object
     */
    public static function DeserializeAttackModes($attackModeData){

        $attackModeObjects = array();        
        
        $attackObjects = array();
        
        foreach($attackModeData as $name => $attacks){
            
            foreach($attacks as $attackData){
                                                
                $attacks = array();
                $damages = array();

                foreach($attackData['damages'] as $damageData){

                    $damages[] = CombatantFactory::DeserializeDamage($damageData);

                }
                
                $a = new Attack( $attackData['range'],
                                 $attackData['type'],
                                 $attackData['base_attack_bonus'],
                                 $attackData['attack_attrs'],
                                 $attackData['attack_bonus'],
                                 $damages,
                                 $attackData['crit_range']);
                                 
                $attacksObjects[] = $a;
            }

            $attackModeObjects[] = new AttackMode($name, $attacksObjects);            
        }
        
        return $attackModeObjects;
    }
    
    /**
     * Returns damage object
     * 
     * @param     array     data from damages
     * 
     * @return    array     Damage objects
     */
    public static function DeserializeDamage($damageData){
        
        if(!isset($damageData['damage_types'])){
            $damageData['damage_types'] = array('BLUDGEONING');
        }
                
        return new Damage( $damageData['damage_die_count'], 
                           $damageData['damage_die_type'], 
                           $damageData['damage_bonus'],
                           $damageData['damage_attrs'], 
                           $damageData['damage_types'],
                           $damageData['damage_attr_multiplier'],                           
                           $damageData['crit_multiplier']);
    }
    
    /**
     * Returns saving throw objects
     * 
     * @param     array    data from saves
     * 
     * @return    array    Saving throw objects
     */
    public static function DeserializeSave($saveData){
                
        return new Save($saveData['type'], $saveData['base'], $saveData['attrs']);
        
    }
    
    /**
     * Returns armor class object
     * 
     * @param    array    data from saves
     * 
     * @return    obj     Armor class object
     */
    public static function DeserializeArmorClass($armorClassBonuses){

        $ac = new ArmorClass(
                        $armorClassBonuses['armorBonus'],
                        $armorClassBonuses['shieldBonus'],
                        $armorClassBonuses['sizeModifier'],
                        $armorClassBonuses['dodgeBonus'],
                        $armorClassBonuses['deflectionBonus'],
                        $armorClassBonuses['naturalArmorBonus'],
                        $armorClassBonuses['maxDexBonus'],
                        $armorClassBonuses['attributes']           
                    );

        return $ac;        
    }
    
    /**
     * Returns an array of names of combatants
     * 
     * @return    array    Array of combatant names
     */
    public static function GetCombatantList(){

        $combatantFiles = scandir(ROOT . DIRECTORY_SEPARATOR . 'combatants');

        $combatantNames = array();

        foreach($combatantFiles as $cf){
            if($cf == '.' || $cf == '..'){
                continue;
            }

            $cf = str_replace('.json', '', $cf);

            $combatantNames[] = $cf;
        }

        return $combatantNames;
    }
}

?>