<?php
                        
$DIE_TYPES = array(
                            '2',
                            '3',
                            '4',
                            '6',
                            '8',
                            '10',
                            '12',
                            '20'
                        );

class Damage{

    private $dieCount = 0;
    private $dieType;    
    private $damageMod;
    private $damageAttrs;
    private $damageTypes;
    private $modMultiplier    = 1;
    private $critMultiplier   = 1;
    
    /**
     * Constructs a damage string
     * 
     * @param   int     dieCount           Number of dice to roll
     * @param   int     dieType            Type of die to roll, 4,6,8 etc
     * @param   int     damageMod          How much damage to add to a roll. +8, -4 etc.
     * @param   array   damageTypes        An array of damage types, such as PIERCING, BLUDGEONING
     * @param   float   modMultiplier      0.5, 1.0, 1.5 etc. 
     * @param   bool    multipliedOnCrit   True if multiplied on crit or not.
     */
    function __construct($dieCount, $dieType, $damageMod, $damageAttrs, $damageTypes, $modMultiplier = 1, $critMultiplier = 1){
        
        $this->dieCount       = $dieCount;
        $this->dieType        = $dieType;
        $this->damageMod      = $damageMod;
        $this->damageAttrs    = $damageAttrs;
        $this->modMultiplier  = $modMultiplier;
        $this->critMultiplier = $critMultiplier;
        
        if(!is_array($damageTypes)){
            $damageTypes = array($damageTypes);
        }
        
        if(!is_array($this->damageAttrs)){
            $this->damageAttrs = array($this->damageAttrs);
        }
                
        $this->damageTypes = $damageTypes;        
    }

    /**
     * Calculates the damage done by this damage string.
     * 
     * @param   obj        combatant                Combatant class object
     * @param   bool       critical                 Was the attack a crit? False if not, true if so
     * 
     * @param   int        Damage done.
     */
    public function CalculateDamage($combatant, $critical){
        
        $amount = 0;
        
        // Die Rolls
        if($critical && $this->critMultiplier != 1){
            $dieCount = $this->dieCount * $this->critMultiplier;
        }else{
            $dieCount = $this->dieCount;
        }
        
        for($rollCount = 0; $rollCount < $dieCount; $rollCount++){
            $amount += rand(1, $this->dieType);
        }
        
        $damageMod     = 0;
        $damageAttrMod = 0;
        
        foreach($this->damageAttrs as $attr){
            if($critical && $this->critMultiplier != 1 && $this->damageMod > 0){
                $damageMod = $damageAttrMod += intval($combatant->AttributeMod($attr) * $this->modMultiplier) * $this->critMultiplier;
            }else{
                $damageAttrMod += intval($combatant->AttributeMod($attr) * $this->modMultiplier);
            }     
        }
                
        // Damage Mod
        if($critical && $this->critMultiplier > 1 && $this->damageMod > 0){
            $damageMod = ($this->damageMod + $damageAttrMod )  * $this->critMultiplier;
        }else{
            $damageMod = $this->damageMod + $damageAttrMod;
        }
        
        $amount += $damageMod + $damageAttrMod;
                
        if($amount < 0){
            $amount = 1;
        }
        
        return array('types' => $this->damageTypes, 'amount' => $amount);      
    }
    
    /**
     * Returns the damamge string. i.e. 2d8+4
     * 
     * @parm    obj        $combatant               Combatant object
     * @param   bool       $critical                TRUE if it was a crit, false if it was not.
     * 
     * @return   str   2d8+4 etc.
     */
     public function GetDamageString($combatant, $critical){
        
        $damageMod     = 0;
        $damageAttrMod = 0;
        
        // CRIT
        if($critical && $this->critMultiplier != 1){
            $dieCount = $this->dieCount * $this->critMultiplier;
        }else{
            $dieCount = $this->dieCount;
        }
        
        // Damage Mod        
        if($critical && $this->critMultiplier != 1 && $this->damageMod > 0){
            $damageMod = $this->damageMod * $this->critMultiplier;
        }else{
            $damageMod = $this->damageMod;
        }                
        
        foreach($this->damageAttrs as $attr){
            if($critical){
                $damageAttrMod += intval( intval($combatant->AttributeMod($attr) * $this->modMultiplier) * $this->critMultiplier );
            }else{
                $damageAttrMod += intval($combatant->AttributeMod($attr) * $this->modMultiplier);
            }     
        }
        
        $damageMod = $damageMod + $damageAttrMod;
        
        return $dieCount . 'd' . $this->dieType . ($damageMod == 0 ? '' : ( $damageMod > 0 ? '+' : '-' ) ) . ($damageMod != 0 ? abs($damageMod) : ''); //. '(' . implode(',', $this->damageTypes) . ')';
        
     }
     
     /**
      * Returns the crit multiplier for this damage
      * 
      * @return    int    Crit multiplier for this damage
      */
     public function CritMultiplier(){
        return $this->critMultiplier;
     }
     
     /**
      * Returns the types of damage for this attack
      * 
      * @return  array
      */
     public function GetDamageTypes(){
        return $this->damageTypes;
     }
}

?>