<?php

class Combatant{
    
    private $knowledgeRollSkillsToCreatureTypes = array(
        'Knowledge (arcana)'        => array( 'CONSTRUCT', 'DRAGON', 'MAGICAL BEAST'                            ),
        'Knowledge (dungeoneering)' => array( 'ABERRATION', 'OOZE'                                              ),
        'Knowledge (local)'         => array( 'HUMANOID'                                                        ),
        'Knowledge (nature)'        => array( 'ANIMAL', 'FEY', 'GIANT', 'MONSTROUS HUMANOID', 'PLANT', 'VERMIN' ),
        'Knowledge (religion)'      => array( 'UNDEAD'                                                          ),
        'Knowledge (the planes)'    => array( 'OUTSIDER', 'ELEMENTAL'                                           )
    );
    
    private $SKILLS = array(
        'Appraise'                                  => 'INT',
        'Balance'                                   => 'DEX',
        'Bluff'                                     => 'CHA',
        'Climb'                                     => 'STR',
        'Concentration'                             => 'CON',
        'Craft'                                     => 'INT',
        'Decipher Script'                           => 'INT',
        'Diplomacy'                                 => 'CHA',
        'Disable Device'                            => 'INT',
        'Disguise'                                  => 'CHA',
        'Escape Artist'                             => 'DEX',
        'Forgery'                                   => 'INT',
        'Gather Information'                        => 'CHA',
        'Handle Animal'                             => 'CHA',
        'Heal'                                      => 'WIS',
        'Hide'                                      => 'DEX',
        'Intimidate'                                => 'CHA',
        'Jump'                                      => 'STR',
        'Knowledge (arcana)'                        => 'INT',
        'Knowledge (architecture and engineering)'  => 'INT',
        'Knowledge (dungeoneering)'                 => 'INT',
        'Knowledge (geography)'                     => 'INT',
        'Knowledge (history)'                       => 'INT',
        'Knowledge (local)'                         => 'INT',
        'Knowledge (nature)'                        => 'INT',
        'Knowledge (nobility and royalty)'          => 'INT',
        'Knowledge (religion)'                      => 'INT',
        'Knowledge (the planes)'                    => 'INT',
        'Listen'                                    => 'WIS',
        'Move Silently'                             => 'DEX',
        'Open Lock'                                 => 'DEX',
        'Perform'                                   => 'CHA',
        'Profession'                                => 'WIS',
        'Ride'                                      => 'DEX',
        'Search'                                    => 'INT',
        'Sense Motive'                              => 'WIS',
        'Sleight of Hand'                           => 'DEX',
        'Spellcraft'                                => 'INT',
        'Spot'                                      => 'WIS',
        'Survival'                                  => 'WIS',
        'Swim'                                      => 'STR',
        'Tumble'                                    => 'DEX',
        'Use Magic Device'                          => 'CHA',
        'Use Rope'                                  => 'DEX'
    );
    
    private $SIZES = array(
                        'FINE'       => 8,
                        'DIMINUTIVE' => 4,
                        'TINY'       => 2,
                        'SMALL'      => 1,
                        'MEDIUM'     => 0,
                        'LARGE'      => -1,
                        'HUGE'       => -2,
                        'GARGANTUAN' => -4,
                        'COLOSSAL'   => -8
                    );
                    
    private $SPECIAL_SIZE_MODIFIERS = array(
                                        'FINE'       => -16,
                                        'DIMINUTIVE' => -12,
                                        'TINY'       => -8,
                                        'SMALL'      => -4,
                                        'MEDIUM'     => 0,
                                        'LARGE'      => 4,
                                        'HUGE'       => 8,
                                        'GARGANTUAN' => 12,
                                        'COLOSSAL'   => 16
                            );
    
    // Name
    private $name = '';
    private $nameTrue = '';
    private $flavorText;
    private $descriptor = '';
        
    // Hit Points
    private $baseHitPoints = 0;
    private $maxHitPoints  = 0;
    private $hitDice       = 0;
    
    private $currentHitPoints = 0;
    
    // Size
    private $sizeCategory = '';
    
    // Alignment    
    private $alignmentLawVsChaos = 'NEUTRAL';
    private $alignmentGoodVsEvil = 'NEUTRAL';
    
    // SPEED
    private $landSpeed   = 0;
    private $swimSpeed   = 0;
    private $flySpeed    = 0;
    private $burrowSpeed = 0;
    private $climbSpeed  = 0;
    
    // TYPE(S)
    private $creatureTypes    = array();
    private $creatureSubTypes = array();
    
    // SKills
    private $skills = array();
    
    // TACTICS AND MORALE
    private $tactics = '';
    private $morale  = '';
    
    // Languages
    private $languages = array();
    
    // Special Abilities
    private $specialAbilities = array();
    
    // Combat Reflexes? (for calculating flat footed AC)
    private $hasCombatReflexes = false;
    private $combatantHasActed = false;
    
    // Temp Hit Points
    private $temporaryHitPoints = 0;

    // Attributes
    private $attributes = array( 'STR' => array( 'ORIGINAL' => 0, 'CURRENT' => 0 ),
                                 'DEX' => array( 'ORIGINAL' => 0, 'CURRENT' => 0 ),
                                 'CON' => array( 'ORIGINAL' => 0, 'CURRENT' => 0 ),
                                 'INT' => array( 'ORIGINAL' => 0, 'CURRENT' => 0 ),
                                 'WIS' => array( 'ORIGINAL' => 0, 'CURRENT' => 0 ),
                                 'CHA' => array( 'ORIGINAL' => 0, 'CURRENT' => 0 ) );

    // Inititiavte
    private $initiativeBonus     = 0;
    private $initiativeAttribute = 'DEX';
    private $initiativeRolled    = array('natural_roll' => 0, 'adjusted_roll' => 0);

    // Damage reduction
    private $damageReduction  = array('types' => array(), 'amount' => 0);
    private $damageResistance = array('types' => array(), 'amount' => 0);

    // Armor Class
    private $armorClass;

    // Combat Gear
    private $combatGear;

    // Saving throws
    private $saves = array();

    // Attack Modes
    private $attackModes = array();

    // Stable
    private $stable = false;

    // Knowledge roll skill
    private $knowledgeRollSkill;
    
    // The number of questions to be answered / knowledge roll
    private $knowledgeRoll = null;
    private $questions     = null;
    private $getsName      = false;      

    /**
     * Instantiates the combat helper.
     * 
     * @param    int      CALCULATED hit points
     */
    public function __construct(  $name, 
                                  $flavorText,
                                  $baseHitPoints,
                                  $sizeCategory,
                                  $alignmentLawVsChaos,
                                  $alignmentGoodVsEvil, 
                                  $landSpeed,
                                  $swimSpeed,
                                  $flySpeed,
                                  $burrowSpeed,
                                  $climbSpeed,
                                  $skills,
                                  $creatureTypes,
                                  $creatureSubTypes,
                                  $hitDice,
                                  $attributes, 
                                  $saves, 
                                  $attackModes, 
                                  $armorClass, 
                                  $tactics              = '',
                                  $morale               = '',
                                  $combatGear           = '',
                                  $hasCombatReflexes    = false,
                                  $initiativeBonus      = 0, 
                                  $languages            = array(),
                                  $specialAbilities     = array(),
                                  $damageReduction      = array(),
                                  $damageResistance     = array(),
                                  $initiativeAttribute  = 'DEX', 
                                  $temporaryHp          = 0,
                                  $descriptor           = ''){

        $this->name                 = $name;
        $this->nameTrue             = $name;
        $this->flavorText           = $flavorText;
        $this->descriptor           = $descriptor;
        $this->baseHitPoints        = $baseHitPoints;
        $this->sizeCategory         = $sizeCategory;
        $this->alignmentLawVsChaos  = $alignmentLawVsChaos;
        $this->alignmentGoodVsEvil  = $alignmentGoodVsEvil;
        $this->landSpeed            = $landSpeed;
        $this->swimSpeed            = $swimSpeed;
        $this->flySpeed             = $flySpeed;
        $this->burrowSpeed          = $burrowSpeed;
        $this->climbSpeed           = $climbSpeed;
        $this->skills               = $skills;
        $this->creatureTypes        = $creatureTypes;
        $this->creatureSubTypes     = $creatureSubTypes;
        $this->hitDice              = $hitDice;
        $this->currentHitPoints     = $this->maxHitPoints;
        $this->attributes           = $attributes;
        $this->saves                = $saves;
        $this->attackModes          = $attackModes;
        $this->armorClass           = $armorClass;
        $this->tactics              = $tactics;
        $this->morale               = $morale;
        $this->combatGear           = $combatGear;
        $this->hasCombatReflexes    = $hasCombatReflexes;
        $this->damageReduction      = $damageReduction;
        $this->damageResistance     = $damageResistance;
        $this->initiativeAttribute  = $initiativeAttribute;
        $this->languages            = $languages;
        $this->initiativeBonus      = $initiativeBonus;
        $this->specialAbilities     = $specialAbilities;
        $this->temporaryHitPoints   = $temporaryHp;
        
        $this->UpdateMaxHitPoints();
        
        if(!is_array($this->damageResistance)){
            $this->damageResistance = array('types' => array(), 'amount' => 0);
        }
        
        if(!is_array($this->damageReduction)){
            $this->damageReduction = array('types' => array(), 'amount' => 0);
        }
        
        $this->currentHitPoints = $this->maxHitPoints + $this->temporaryHitPoints;
        $this->SetKnowledgeRollSkill();
    }
    
    /**
     * Rename this combatant with a count. Just in case an encounter has several of the same combatant
     * 
     * @param    int    count    which combatant is this? 1, 2, 3, 4 etc.
     */
    public function RenameByCount($count){
        $this->name = $this->name . ' (' . $count . ')';
    }
    
    /**
     * Rolls initiative!
     * 
     * @param     int    Extra bonuses to the roll
     * @param     int    Natural roll in case we want to roll irl.
     * 
     * @return    int    Initiative roll
     */
    public function RollInitiative($extraBonuses = 0, $naturalRoll = null){
        
        if(is_null($naturalRoll)){
            $naturalRoll = rand(1, 20);
        }
        
        $mod = $extraBonuses + $this->InitBonus();
        
        $adjustedRoll = $naturalRoll + $mod;
        
        $this->initiativeRolled = array('natural_roll' => $naturalRoll, 'adjusted_roll' => $adjustedRoll); 
        
        return $this->initiativeRolled;
    }
    
    /**
     * Make an attack!
     *
     * $param    str    title of the attack mode
     */
    public function Attack($attackModeTitle, $targetAc, $naturalAttackRoll = 0, $naturalConfirmRoll = 0, $extraAttackBonus = 0, $damageReductionTypes = array(), $damageReductionAmount = 0){
     
        $attackMode = null;
        
        foreach($this->attackModes as $mode){
            if($mode->Title() == $attackModeTitle){
                $attackMode = $mode;
                break;
            }
        }
        
        return $attackMode->MakeAttack($targetAc, $extraAttackBonus, $damageReductionTypes, $damageReductionAmount, $naturalAttackRoll, $naturalConfirmRoll);
    }
    
    /**
     * Return array of attack mode options
     * 
     * @return   array
     */
    public function GetAttackModes(){

        $modes = array();

        foreach($this->attackModes as $mode){
            $modes[] = array( 'type'          => $mode->AttackType(),
                              'title'         => $mode->Title(),
                              'attack_string' => $mode->AttackString($this) ); 
        }

        return $modes;
    }
    
    /**
     * Gets a <br /> separated string of all attack modes
     * 
     * @return     string    Attack Mode string
     */
    public function GetAttackModesString(){
        
        $modes = $this->GetAttackModes();
        
        $ret = array();
        
        foreach($modes as $m){
            $ret[] = '<strong>' . ucwords(strtolower(str_replace('_', ' ', $m['type']))) . '</strong> ' . $m['title'] . ' ' . $m['attack_string'];
        }
        
        return implode('<br />', $ret);
    }
    
    /**
     * Make an attack with the selected mode
     * 
     * @param    str    attack mode title
     * @param    int    extra bonus to attack roll
     * @param    int    natural roll if we want to input a real life roll
     * @param    int    natural confirmation roll if we want to input a real life roll
     * 
     * @return   array  Array with information on the attack
     */
     /*
    public function Attack($modeTitle, $targetAc, $extraBonuses = 0, $damageReductionTypes = array(), $damageReductionAmount = 0, $naturalAttackRoll = null, $naturalConfirmationRoll = null){

        foreach($this->attackModes as $mode){
            if($mode->Title() == $modeTitle){
                break;
            }
        }

        return $mode->MakeAttack($targetAc, $extraBonuses, $damageReductionTypes, $damageReductionAmount, $naturalAttackRoll, $naturalConfirmationRoll);
    }
    */

    /**
     * Get current size modifier
     * 
     * @return    int    Size modifier
     */
    public function SizeModifier(){        
        return $this->SIZES[$this->sizeCategory];
    }

    /**
     * Get current attribute bonus
     * 
     * @param    str    type   STR,DEX,CON,INT,WIS,CHA
     * 
     * @return   int    The mod for the attribute.
     */
    public function AttributeMod($type){
        
        if($type == 'NONE' || $this->attributes[$type]['CURRENT'] == null || $this->attributes[$type]['CURRENT'] == 'null'){
            return 0;
        }

        $mod = floor( intval( ($this->attributes[$type]['CURRENT'] - 10) ) / 2 );

        return $mod;
    }
    
    /**
     * Get current attribute score
     * 
     * @param    str    type   STR,DEX,CON,INT,WIS,CHA
     * 
     * @return   int     The score for the attribute
     */
    public function AttributeScore($type){
        return $this->attributes[$type]['CURRENT'];
    }
    
    /**
     * Get original attribute score
     * 
     * @param    str    type   STR,DEX,CON,INT,WIS,CHA
     * 
     * @return   int    The original score for the attribute
     */
    public function AttributeScoreOriginal($type){
        return $this->attributes[$type]['ORIGINAL'];
    }
    
    /**
     * Get BAB
     * 
     * @return    int    Base Attack Bonus
     */
    public function BaseAttackBonus(){
        
        $highestBab = 0;
        
        foreach($this->attackModes as $mode){
            
            $testBab = intval($mode->HighestBab());
                        
            if($testBab > $highestBab){
                $highestBab = $testBab;
            }
        }
        
        return $highestBab;
    }
    
    /**
     * Get BAB with + or -
     * 
     * @return   str
     */
    public function BaseAttackBonusString(){

        $highestBab = $this->BaseAttackBonus();

        return ($highestBab >= 0 ? '+' : '') . $highestBab;        
    }

    /**
     * Get grapple bonus
     * 
     * @return    int    Grapple Bonus
     */
    public function GrappleBonus(){
       return $this->BaseAttackBonus() + $this->AttributeMod('STR') + $this->SPECIAL_SIZE_MODIFIERS[$this->sizeCategory]; 
    }

    /**
     * Get grapple bonus as a string
     * 
     * @return    str    Grapple Bonus
     */
    public function GrappleBonusString(){

        $grappleBonus = $this->GrappleBonus();

        return ($grappleBonus >= 0 ? '+' . $grappleBonus : $grappleBonus);
    }
    
    /**
     * Returns the combatants Armor Class
     * 
     * @param     str    type    The type of attack: MELEE, MELEE_TOUCH, RANGED, RANGED_TOUCH, 'FLAT_FOOTED'
     * 
     * @return    int    Armor Class
     */
    public function ArmorClass($type = 'MELEE'){
        
        //echo $this->armorClass->ArmorClass($this) . '<br />';
        //echo $this->armorClass->TouchArmorClass($this) . '<br />';
        //echo $this->armorClass->FlatFootedArmorClass($this) . '<br />';
        
        switch($type){
            case('MELEE'):
            case('RANGED'):
                return $this->armorClass->ArmorClass($this);
            break;
            
            case('MELEE_TOUCH'):
            case('RANGED_TOUCH'):
                return $this->armorClass->TouchArmorClass($this);
            break;
            
            case('FLAT_FOOTED'):
                return $this->armorClass->FlatFootedArmorClass($this);
            break;
            
            default:
                return $this->armorClass->ArmorClass($this);
            break;
        }
    }
    
    /**
     * Make a saving throw!
     * 
     * @param    str    type            REF | FORT | WILL
     * @param    int    extraBonuses    Temporary bonuses such as from spells or whatever
     * @param    int    naturalRoll     In case the player wants to roll IRL
     * 
     * @param    array  Effect of a saving throw
     */
    public function MakeSavingThrow($type, $targetDc, $extraBonuses = 0, $naturalRoll = null){
        
        $result = $this->saves[$type]->MakeSavingThrow($this, $targetDc, $extraBonuses, $naturalRoll);
        
        return $result;
    }
    
    /**
     * Reset attack counters for each attack mode. Usually done at the end / beginning of a round.
     */
    public function ResetAttackCounter(){
        
        foreach($this->attackModes as $mode){
            $mode->ResetAttackCounter();
        }
    }
    
    /**
     * Returns the combatants current hit points
     * 
     * @return    int    Current hit points
     */
    public function HitPoints(){
        
        $hp = $this->currentHitPoints + $this->temporaryHitPoints;
        
        return $hp;
        
    }
    
    /**
     * Returns the combatants current Hit Points and the string that created it
     * 
     * @return    str    Current hit points string
     */
    public function HitPointsString(){
        return $this->HitPoints() . '/' . $this->maxHitPoints . 
                '(' . $this->hitDice . 'hd' . ' + ' . $this->baseHitPoints . 
                ($this->temporaryHitPoints > 0 ? $this->temporaryHitPoints . ' tempHp' : '') .  ')';
    }

    
    /**
     * Calculates max hit points
     * 
     * @return    int     Max Hit Points
     */
    private function UpdateMaxHitPoints(){
        $maxHp = ( $this->hitDice * $this->AttributeMod('CON') ) + $this->baseHitPoints;

        $this->maxHitPoints = $maxHp; 
    }
    
    /**
     * If this combatant is Conscious or not
     * 
     * @return    bool    True if the combatant has 1 or more HP. False if 0 (disabled), -1 -9 (dying), -10 or < dead
     */
    public function IsConscious(){
        return $this->currentHitPoints > 0 && $this->AttributeScore('CON') > 0;
    }
    
    /**
     * If this combatant is disabled or not
     * 
     * @return    bool   True if HP is exactly 0
     */
    public function IsDisabled(){
        return $this->currentHitPoints == 0 && $this->AttributeScore('CON') > 0;
    }

    /**
     * If this combatant is dying
     * 
     * @return   bool    True if HP is -1 through -9
     */
    public function IsDying(){
        return !$this->stable && $this->currentHitPoints < 0 && $this->currentHitPoints > -10 && !$this->IsDead();
    }
    
    /**
     * Stabilize: Make a stabilizing roll
     * 
     * @param     int     naturalRoll    1-100
     * 
     * @return    bool    True if stabilized false if not
     */
    public function Stabilize($naturalRoll = null){
        
        if(is_null($naturalRoll)){
            $naturalRoll = rand(1, 100);
        }

        if($naturalRoll >= 91){
            $this->stable = true;
        }else{
            $this->stable = false;
            
            $this->TakeHpDamage( array(
                                        array('amount' => 1, 'types' => array('UNTYPED'))
                                    ));
        }

        return $this->stable;
    }
    
    /**
     * If this combatant is stable somewhere between -1 and -9
     * 
     * @return   bool   true if HP is -1 through -9 and combatant is not dead
     */
    public function IsStable(){
        return $this->stable && !$this->IsDead() && $this->currentHitPoints < 0 && $this->currentHitPoints > -10;
    }
    
    /**
     * If this combatant is dead
     *
     * @return    bool    True if HP is -10 or less
     */
    public function IsDead(){
        return $this->currentHitPoints <= -10 || ( $this->AttributeScore('CON') <= 0 && $this->AttributeScore('CON') != null );
    }
    
    /**
     * Is this combatant flat footed?
     * 
     * @return    bool    True if they are flat footed, false otherwise
     */
    public function IsFlatfooted(){
        
        if($this->hasCombatReflexes){
            return false;
        }
        
        if($this->combatantHasActed){
            return false;
        }
        
        return true;
    }
    
    /**
     * Set that the combatant has acted during a combat round
     */
    public function SetCombatantHasActed(){
        $this->combatantHasActed = true;
    }
    
    
    /**
     * Reduces HP from an attack
     * 
     * @param     array   damages      array('amount' => int, 'types' => array('PIERCING', 'FIRE'))
     */
    public function TakeHpDamage($damages){
        
        $damageTaken = $this->CalculateHpDamageTaken($damages);
        
        $damageTakenTotal = 0;
        
        foreach($damageTaken as $d){
            $damageTakenTotal += $d['amount_taken'];
        }
        
        if($this->temporaryHitPoints > 0){
            if($this->temporaryHitPoints - $damageTakenTotal >= 0){
                $this->temporaryHitPoints = $this->temporaryHitPoints - $damageTakenTotal;
                return;
            }else{
                
                $damageTakenTotal = $damageTakenTotal - $this->temporaryHitPoints;
                
                $this->temporaryHitPoints = 0;
            }
        }
        
        $this->currentHitPoints = $this->currentHitPoints - $damageTakenTotal;        
    }
        
    /**
     * Calculates HP damage taken before reducing any amount of health.
     * 
     * @param     array   damages      array('amount' => int, 'types' => array('PIERCING', 'FIRE'))
     * 
     * @return    array   ('types' => array(), 'amount' => int)
     */
    private function CalculateHpDamageTaken($damages){
        $total = array();
                
        foreach($damages as $damage){
            
            $damageAmount = $damage['amount'];
            $types        = $damage['types'];
            
            $damageReductionAmount  = 0;
            $damageResistanceAmount = 0;
            
            
            if(count($this->damageResistance) > 0){
                foreach($this->damageResistance as $dr){
                    foreach($dr['types'] as $drType){
                        if(in_array($drType, $types)){
                            $damageResistanceAmount = $dr['amount'];
                        }
                    }
                }
            }            
            
            if(count($this->damageReduction) > 0){
                foreach($this->damageReduction as $dr){
                    
                    $foundDrType   = 0;
                    
                    foreach($dr['types'] as $drType){
                        if(in_array($drType, $types)){
                            $foundDrType++;
                        }
                    }
                
                    if($foundDrType != count($dr['types'])){
                        $damageReductionAmount += $dr['amount'];
                    }
                }        
            }
                
            
            $damageAmount = $damageAmount - $damageResistanceAmount;            
            $damageAmount = $damageAmount - $damageReductionAmount;
            
            if($damageAmount < 0){
                $damageAmount = 0;
            }
            
            $total[] = array('types' => $damage['types'], 'amount_taken' => $damageAmount, 'amount_reduced' => $damageReductionAmount, 
                             'amount_resisted' => $damageResistanceAmount);
        }
        
        return $total;
    }
    
    /**
     * Returns an array of skills modified by their correct attributes
     * 
     * return    array    array of skills
     */
    public function Skills(){
        
        $skillsRet = array();
        
        foreach($this->skills as $skill){
                        
            $name  = $skill['skill'];
            $bonus = $skill['bonus'];
            $mod   = $this->AttributeMod( $this->SKILLS[$skill['skill']] );
            
            $skillsRet[$skill['skill']] = array(
                                            'name'          => $name,
                                            'bonus'         => $bonus,
                                            'attr'          => $this->SKILLS[$skill['skill']],
                                            'mod'           => $mod,
                                            'total'         => $bonus + $mod,
                                            'total_display' => ($bonus + $mod >= 0 ? '+' . ($bonus + $mod) : '' . ($bonus + $mod))
                                        );
        }
        
        if(!isset($skillsRet['Listen'])){
            
            $mod = $this->AttributeMod( $this->SKILLS['Listen'] );
            
            $skillsRet['Listen'] = array(
                                        'name'          => 'Listen',
                                        'bonus'         => 0,
                                        'attr'          => $this->SKILLS['Listen'],
                                        'mod'           => $mod,
                                        'total'         => 0 + $mod,
                                        'total_display' => (0 + $mod >= 0 ? '+' . $mod : '' . $mod)
                                    );
        }
        
        if(!isset($skillsRet['Spot'])){
            $mod = $this->AttributeMod( $this->SKILLS['Spot'] );
            
            $skillsRet['Spot'] = array(
                                        'name'          => 'Spot',
                                        'bonus'         => 0,
                                        'attr'          => $this->SKILLS['Spot'],
                                        'mod'           => $mod,
                                        'total'         => 0 + $mod,
                                        'total_display' => (0 + $mod >= 0 ? '+' . $mod : '' . $mod)
                                    );
        }
        
        return $skillsRet;
        
    }
    
    /**
     * Gets the name of this combatant
     * 
     * @return   string   Name of the combatant
     */
    public function Name(){
        return $this->name;
    }
    
    /**
     * Gets the name of this combatant without numbering for multiple combatants of the same kind
     * 
     * @return    string   True name of the combatant
     */
    public function NameTrue(){
        return $this->nameTrue;
    }
    
    public function InitBonus(){
        return $this->AttributeMod($this->initiativeAttribute) + $this->initiativeBonus;
    }
    
    public function SaveBonuses(){
        
        $breakDowns = array();
        
        foreach($this->saves as $s){
            $breakDowns[] = $s->GetSaveBreakDown($this);
        }
        
        return $breakDowns;
    }
    
    /**
     * Get a string to display saves and their bonuses
     * 
     * @return   str  
     */
    public function SaveBonusesString(){
        $breakDowns = $this->SaveBonuses();

        $ret = '';

        $ret .= '<strong>' . ucwords(strtolower($breakDowns[1]['type'])) . ':</strong> ' . (intval($breakDowns[1]['total_bonus'] >= 0) ? '+' : '') . $breakDowns[1]['total_bonus'] . ' ';
        $ret .= '<strong>' . ucwords(strtolower($breakDowns[0]['type'])) . ':</strong> ' . (intval($breakDowns[0]['total_bonus'] >= 0) ? '+' : '') . $breakDowns[0]['total_bonus'] . ' ';
        $ret .= '<strong>' . ucwords(strtolower($breakDowns[2]['type'])) . ':</strong> ' . (intval($breakDowns[2]['total_bonus'] >= 0) ? '+' : '') . $breakDowns[2]['total_bonus'] . ' ';

        return $ret;
    }
    
    public function SpeedString(){
        
        $ret = array();
                
        if(intval($this->landSpeed) != 0){
            $ret[] = $this->landSpeed . ' ft.';
        }
        
        if(intval($this->swimSpeed) != 0){
            $ret[] = 'swim ' . $this->swimSpeed . ' ft.';
        }
        
        if(intval($this->flySpeed) != 0){
            $ret[] = 'fly ' . $this->flySpeed . ' ft.';
        }
        
        if(intval($this->burrowSpeed) != 0){
            $ret[] = 'burrow ' . $this->burrowSpeed . ' ft.';
        }
        
        if(intval($this->climbSpeed) != 0){
            $ret[] = 'climb ' . $this->climbSpeed . ' ft.';
        }
        
        $ret = implode(', ', $ret);
        
        return $ret;
    }
    
    /**
     * Returns descriptor text
     */
    public function Descriptor(){
        return $this->descriptor;
    }
    
    /**
     * Returns alignment short hand
     */
    public function Alignment(){
        
        $lawVsChaos = substr($this->alignmentLawVsChaos, 0, 1);
        $goodVsEvil = substr($this->alignmentGoodVsEvil, 0, 1);
        
        if($lawVsChaos == 'N' && $goodVsEvil == 'N'){
            return 'N';
        }
        
        return $lawVsChaos . $goodVsEvil;
    }
    
    /**
     * Returns creature size
     */
    public function Size(){
        return ucwords(strtolower($this->sizeCategory));
    }
    
    /**
     * Returns the creature type and subtype
     */
    public function CreatureType(){
        $creatureTypes = implode(', ', $this->creatureTypes);
        
        if(count($this->creatureSubTypes) > 0 && $this->creatureSubTypes[0] != 'NONE'){
            $subTypes = implode(', ', $this->creatureSubTypes);    
        }else{
            $subTypes = '';
        }
        
        return strtolower( $creatureTypes . ($subTypes == '' ? '' : ' (' . $subTypes . ')') );        
    }
    
    /**
     * Returns tactics string
     */
    public function Tactics(){
        return $this->tactics;
    }
    
    /**
     * Returns morale stringe
     */
    public function Morale(){
        return $this->morale;
    }
    
    /**
     * Returns an array of languages
     */
    public function Languages(){
        return $this->languages;
    }
    
    /**
     * Returns the gear string of the combatant
     */
    public function Gear(){
        return $this->combatGear;
    }
    
    /**
     * Returns the special abilities of the combatant
     */
    public function SpecialAbilities(){
        return $this->specialAbilities;
    }
    
    /**
     * Returns the name of the skill that is needed to roll knowledge for this creature.
     */
    public function KnowledgeRollSkill(){
        return $this->knowledgeRollSkill;
    }
    
    /**
     * Sets this creatures knowledge roll skill
     */
    private function SetKnowledgeRollSkill(){
        
        foreach($this->knowledgeRollSkillsToCreatureTypes as $skill => $set){
            foreach($this->creatureTypes as $type){
                if(in_array($type, $set)){
                    $this->knowledgeRollSkill = $skill;
                    break;
                } 
            }
        }
        
    }
    
    /**
     * Makes a knowledge roll
     *
     * @param   int   Roll that the character made WITH BONUSES
     * 
     * @return  int   How many questions the character gets to ask
     */
    public function MakeKnowledgeRoll($totalRoll){
        
        $dc = 10 + $this->hitDice;
        
        $questions = intval( ($totalRoll - $dc) / 5 );
        
        $this->knowledgeRoll = $totalRoll;
        $this->questions     = $questions;
        $this->getsName      = ( ($totalRoll - $dc) >= 0 );
        
        return $questions;        
    }
    
    /**
     * Determines if a knowledge roll was made or not
     * 
     * @return   bool    true if roll was made, false otherwise.
     */
    public function KnowledgeRollMade(){
        return !is_null($this->knowledgeRoll);
    }
    
    /**
     * returns the number of questions the character gets
     * 
     * @return  int   
     */
    public function Questions(){
        return $this->questions;
    }
    
    /**
     * returns whether or not the character knows the monsters name from their knowledge roll
     * 
     * @return  bool   true if name is known, false otherwise.
     */
    public function GotName(){
        return $this->getsName;
    }
    
}

?>