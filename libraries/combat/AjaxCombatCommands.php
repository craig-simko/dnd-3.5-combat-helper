<?php

include_once('../../config.php');
include_once('../../inc.php');

session_start();

$command = $_POST['command'];
$data    = json_decode($_POST['data'], true);

$combat  = $_SESSION['activeCombat'];

switch($command){

    case('RollInitiative'):
        RollInitiative($combat, $data);
    break;
    
    case('EndTurn'):
        EndTurn($combat, $data);
    break;

    case('GetCombatantView'):
        GetCombatantView($combat, $data);
    break;
    
    case('Attack'):
        Attack($combat, $data);
    break;
    
    case('TakeDamage'):
        TakeDamage($combat, $data);
    break;
    
    case('DelayCombatant'):
        Delay($combat, $data);
    break;
    
    case('RemoveCombatant'):
        Remove($combat, $data);
    break;
    
    case('AddCombatantBackToInitiativeOrder'):
        AddCombatantBackToInitiativeOrder($combat, $data);
    break;
    
    case('AddNewCombatant'):
        AddNewCombatant($combat, $data);
    break;
    
    case('AddRoundTimer'):
        AddRoundTimer($combat, $data);
    break;
    
    case('MakeKnowledgeRoll'):
        MakeKnowledgeRoll($combat, $data);
    break;
}

/**
 * Issues a roll init command to the active combat
 * 
 * @param    array    inits
 */
function RollInitiative($combat, $inits){
      
   $combat->Initiative($inits);
   
   echo $combat->Display();
   
}

/**
 * Ends the turn of a combatant
 */
function EndTurn($combat){
    
    $combat->EndTurn();
    
    echo $combat->Display();
}

/**
 * gets full view of a combatant
 */
function GetCombatantView($combat, $combatantName){
    
    $combat->DisplayCombatant($combatantName);
    
}

/**
 * Makes Combatant take damage
 */
function TakeDamage($combat, $combatantNameAndDamageAmount){
        
    $combatantName = $combatantNameAndDamageAmount['combatant_name'];
    $damage        = $combatantNameAndDamageAmount['damage_amount'];
    
    $combat->TakeDamage($combatantName, $damage);
    
    echo $combat->Display();
}

/**
 * Attack!
 */
function Attack($combat, $attackData){
    //var_dump($attackData);
}

/**
 * Sets a combatant to delay
 */
function Delay($combat, $combatant){

    $combat->DelayCombatant($combatant['combatant_name']);

    echo $combat->Display();
}

/**
 * Removes a combatant from the initiative order
 */
function Remove($combat, $combatant){
    $combat->RemoveCombatant($combatant['combatant_name']);

    echo $combat->Display();
}

/**
 * Removes a combatant from the initiative order
 */
function AddCombatantBackToInitiativeOrder($combat, $combatant){
    $combat->AddCombatantBackToInitiativeOrder($combatant['combatant_name']);

    echo $combat->Display();
}

/**
 * Removes a combatant from the initiative order
 */
function AddNewCombatant($combat, $combatantAndCount){
    $combat->AddNewCombatant($combatantAndCount['combatant_name'], $combatantAndCount['count']);

    echo $combat->Display();
}

/**
 * Adds a round timer to the combat
 */
function AddRoundTimer($combat, $detailsAndRoundTrigger){
    $combat->AddRoundTimer($detailsAndRoundTrigger['details'], $detailsAndRoundTrigger['round_trigger']);
    
    echo $combat->Display();
}

/**
 * Makes a knowledge roll and returns how many questions the character gets to ask the answers to.
 */
function MakeKnowledgeRoll($combat, $rollAndTarget){
    
    $combatantBeingInspectedName = $rollAndTarget['combatant_name'];
    $knowledgeRoll               = $rollAndTarget['knowledge_roll'];
    
    $questions = $combat->MakeKnowledgeRoll($combatantBeingInspectedName, $knowledgeRoll);
    
    echo $combat->Display();
}

?>