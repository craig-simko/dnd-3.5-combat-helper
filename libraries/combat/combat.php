<?php

function SortInitOrder($a, $b){
    
    $initA = $a['adjusted_roll'];
    $initB = $b['adjusted_roll'];

    if($initA == $initB){
        $initDiffA = $a['adjusted_roll'] - $a['natural_roll'];
        $initDiffB = $b['adjusted_roll'] - $b['natural_roll'];
        
        if($initDiffA != $initDiffB){
            return $initDiffA < $initDiffB;
        }
        
        $initA = 0;
        $initB = 0;
        
        while($initA == $initB){
            $initA = rand(1, 20) + $initDiffA;
            $initB = rand(1, 20) + $initDiffB;
        }
    }
    
    if($initA < $initB){
        return 1;
    }else{
        return 0;
    }
    
}

class Combat{
    
    private $combatants        = array();
    private $combatantsOnDelay = array();
    private $combatantsRemoved = array();
    private $round = 1;
    private $firstCombatant = '';
    private $newCombatantGroupCount = 0;
    private $initiativeRolled = false;
    private $log = array();
    private $roundTimer = array();
    
    /**
    * Instantiates an attack
    * 
    * @param    array    combatants    an array of combatant objects
    */  
    public function __construct($combatants) {

        $this->combatants = $combatants;
        
        if(!is_array($this->combatants)){
            $this->combatants = array();
        }

    }
    
    /**
     * Returns list of combatants and their indexes for this class
     * 
     * @return    array    List of combatants by name.
     */
    public function CombatantList(){

       $combatantList = array();

       foreach($this->combatants as $key => $c){
            $combatantList[$key] = $c->Name();
       }

       return $combatantList;        
    }
    
    /**
     * Roll init
     */
    public function Initiative($naturalRolls = array()){

        $inits = array();

        foreach($this->combatants as $key => $c){

            $naturalRoll = null;

            if(isset($naturalRolls[$key]) && $naturalRolls[$key] != ''){
                $naturalRoll = $naturalRolls[$key];
            }

            $init = $c->RollInitiative(0, $naturalRoll);
            $inits[] = array(   'natural_roll'  => $init['natural_roll']
                              , 'adjusted_roll' => $init['adjusted_roll']
                              , 'combatant'     => $c->Name()
                            );

        }

        usort($inits, "SortInitOrder");

        $logText = 'Initiative Rolled:<br />';

        $newCombatantListOrderedByInit = array();

        foreach($inits as $i){
            
            $name = $i['combatant'];
            
            foreach($this->combatants as $c){
                if($c->Name() == $name){
                    $newCombatantListOrderedByInit[] = $c;
                    
                    $logText .= '&nbsp;&nbsp;&nbsp;&nbsp;' . $name . ' rolled: ' . $i['adjusted_roll'];
                    
                    break;
                }
            }
        }
        
        $this->log($logText, 'INFO');
        
        $this->log('Round Start: 1');
        
        $this->LogTimers();
        
        $this->initiativeRolled = true;
        
        $this->combatants = $newCombatantListOrderedByInit;
        
        $this->firstCombatant = $this->combatants[0]->Name();
        
        $this->log('Turn Start: ' . $this->firstCombatant);
    }
    
    /**
     * Ends the current combatants turn
     */
     public function EndTurn(){
        
        reset($this->combatants);
        $first_key = key($this->combatants);

        $tempCombatant = $this->combatants[$first_key];

        $this->log('Turn End: ' . $tempCombatant->Name());

        unset($this->combatants[$first_key]);
        array_push($this->combatants, $tempCombatant); 
        
        reset($this->combatants);
        $first_key = key($this->combatants);
        
        if($this->combatants[$first_key]->Name() == $this->firstCombatant || count($this->combatants) <= 1){
            $this->RoundEnd();
        }

        $this->log('Turn Start: ' . $this->combatants[$first_key]->Name());
     }
     
     /**
      * Ends a round
      */
     public function RoundEnd(){
        $this->log('Round End: ' . $this->round);
        
        $this->round++;
        
        $this->log('Round Start: ' . $this->round);
        
        $this->LogTimers();
        
     }
     
    /**
     * Determines the new first combatant
     */
    private function UpdateFirstCombatant(){
        foreach($this->combatants as $key => $c){
            $this->firstCombatant = $c->Name();
            break;
        }
    }
     
    /**
     * Delay Combatant
     * 
     * @param   str   combatant name
     */
    public function DelayCombatant($name){
        foreach($this->combatants as $key => $c){
            if($c->Name() == $name){
                
                $combatant = $this->combatants[$key];
                
                unset($this->combatants[$key]);
                
                $this->combatantsOnDelay[] = $combatant;
                
                $this->log( $name . ' delayed.');                
                
                break;
            }
        }
        
        $this->UpdateFirstCombatant();
    }
    
    /**
     * Remove Combatant
     * 
     * @param   str   combatant name
     */
    public function RemoveCombatant($name){
        foreach($this->combatants as $key => $c){
            if($c->Name() == $name){
                
                $combatant = $this->combatants[$key];
                
                unset($this->combatants[$key]);
                
                $this->combatantsRemoved[] = $combatant;
                
                $this->log( $name . ' removed from combat.');                
                
                break;
            }
        }
        
        foreach($this->combatantsOnDelay as $key => $c){
            if($c->Name() == $name){
                
                $combatant = $this->combatantsOnDelay[$key];
                
                unset($this->combatantsOnDelay[$key]);
                
                $this->combatantsRemoved[] = $combatant;
                
                $this->log( $name . ' removed from combat.');                
                
                break;
            }
        }
        
        $this->UpdateFirstCombatant();
    }
    
    /**
     * Add a NEW combatant to the initiave order
     * 
     * @param   str   combatant name
     * @param   int   how many to add to the initiative
     */
    public function AddNewCombatant($combatantName, $count){

        $count = intval($count);

        $this->newCombatantGroupCount++;

        if($count < 1){
            $count = 1;
        }

        for($i = 0; $i < $count; $i++){
            $newCombatant = CombatantFactory::CreateCombatantByName($combatantName);
            
            $newCombatant->RenameByCount( $this->newCombatantGroupCount . '.' . ($i + 1) );
                        
            array_unshift ( $this->combatants , $newCombatant );
            
            $this->log($newCombatant->Name() . ' entered the combat.');
        }
        
        $this->UpdateFirstCombatant();
    }
    
    
    /**
     * Add Combatant back to initiative order
     * 
     * @param   str   combatant name
     */
    public function AddCombatantBackToInitiativeOrder($name){
        foreach($this->combatantsRemoved as $key => $c){
            if($c->Name() == $name){
                
                $combatant = $this->combatantsRemoved[$key];
                
                unset($this->combatantsRemoved[$key]);
                
                array_unshift ( $this->combatants , $combatant );
                
                $this->log( $name . ' returns to combat.');                
                
                break;
            }
        }
        
        foreach($this->combatantsOnDelay as $key => $c){
            if($c->Name() == $name){

                $combatant = $this->combatantsOnDelay[$key];

                unset($this->combatantsOnDelay[$key]);

                array_unshift ( $this->combatants , $combatant );

                $this->log( $name . ' undelays.');                

                break;
            }
        }
        
        $this->UpdateFirstCombatant();
    }

    /**
     * Causes a combatant specified to take that much damage
     */
    public function TakeDamage($combatantName, $damageAmount){
       
       $damageAmount = intval($damageAmount);
       
       if($damageAmount < 0){
            $action = 'healing';
       }else{
            $action = 'damage';
       }
       
       $this->log($combatantName . ' takes ' . abs($damageAmount) . ' point' . (abs($damageAmount) == 1 ? '' : '') . ' of <em>' . $action . '</em>.');
       
       $damages = array(
                    array('amount' => $damageAmount, 'types' => array('UNTYPED'))
                );
       
       foreach($this->combatants as $c){
            if($combatantName == $c->Name()){
                $c->TakeHpDamage($damages);
                break;
            }
        }
        
        foreach($this->combatantsOnDelay as $c){
            if($combatantName == $c->Name()){
                $c->TakeHpDamage($damages);
                break;
            }
        }
        
        foreach($this->combatantsRemoved as $c){
            if($combatantName == $c->Name()){
                $c->TakeHpDamage($damages);
                break;
            }
        }
        
    }
     
    /**
     * Attack!
     */
    public function AttackCombatant($attackerName, $defenderName, $attackModeTitle, $attackType, $naturalAttackRoll = 0, $naturalConfirmRoll = 0, $damage = 0, $extraAttackBonus = 0){
        
        $attacker = null;
        $defender = null;
                
        // In init order
        foreach($this->combatants as $c){
            if($attackerName == $c->Name()){
                $attacker = $c;
            }
            
            if($defenderName == $c->Name()){
                $defender = $c;
            }
            
            if(!is_null($attacker) && !is_null($defender)){
                $targetAc = $defender->ArmorClass($attackType);
        
                $attack   = $attacker->MakeAttack($attackModeTitle, $targetAc, $naturalAttackRoll, $naturalConfirmRoll, $extraAttackBonus);
                break;
            }
        }
        
        $attacker = null;
        $defender = null;
        
        // on delay
        foreach($this->combatantsOnDelay as $c){
            if($attackerName == $c->Name()){
                $attacker = $c;
            }
            
            if($defenderName == $c->Name()){
                $defender = $c;
            }
            
            if(!is_null($attacker) && !is_null($defender)){
                $targetAc = $defender->ArmorClass($attackType);
        
                $attack   = $attacker->MakeAttack($attackModeTitle, $targetAc, $naturalAttackRoll, $naturalConfirmRoll, $extraAttackBonus);
                break;
            }
        }
        
        $attacker = null;
        $defender = null;
        
        // combatants out of combat
        foreach($this->combatantsRemoved as $c){
            if($attackerName == $c->Name()){
                $attacker = $c;
            }

            if($defenderName == $c->Name()){
                $defender = $c;
            }

            if(!is_null($attacker) && !is_null($defender)){
                $targetAc = $defender->ArmorClass($attackType);

                $attack   = $attacker->MakeAttack($attackModeTitle, $targetAc, $naturalAttackRoll, $naturalConfirmRoll, $extraAttackBonus);
                break;
            }
        }        
    }

    /**
     * Returns the display of the combat
     */
     public function Display(){

        $combatants = $this->combatants;

        if(!$this->initiativeRolled){

            $combatants = $this->CombatantList();

            include_once(ROOT . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'combat' . DIRECTORY_SEPARATOR . 'GetInitiative.php');
            return;
        }
        
        ?>
        
        <div class="combat-round-display">
            Round: <?php echo $this->round; ?>
        </div>
        
        <?php
        
        $this->DisplayRoundTimers();

        if(count($combatants) > 0):

            ?>
            <div class="panel panel-default combat-container">
            <?php
            $this->DisplayCombatantTable($combatants);
            ?>
            </div>
            <?php

        endif;


        $combatantsOnDelay = $this->combatantsOnDelay;

        if(count($combatantsOnDelay) > 0):
            
            ?>
            <div class="panel panel-default combat-container">
                <div class="combatatant-on-delay-header">
                    On Delay:
                </div>
            
            <?php
        
            $this->DisplayCombatantTable($combatantsOnDelay, false, true);
            
            ?>
            </div>
            <?php
        
        endif;
        
        $combatantsRemoved = $this->combatantsRemoved;
                
        if(count($combatantsRemoved) > 0):
            
            ?>
            <div class="panel panel-default combat-container">
                <div class="combatatant-out-of-combat-header">
                    Out of Combat:
                </div>
            
            <?php
        
            $this->DisplayCombatantTable($combatantsRemoved, false, false);
            
            ?>
            </div>
            <?php
            
        endif;
        
        ?>
        
        <div class="combatant-view-container">
            
        </div>
        
        <div class="combat-log-header">
            Combat Log:
        </div>
        
        <div class="combat-log-container">
            <?php
            foreach($this->log as $round => $messages):
            
                foreach($messages as $key => $message):    
                ?>
                    <div class="combat-log-item">
                        <?php echo $message['message']; ?>
                    </div>
                <?php
                endforeach;
            endforeach;
            ?>
        </div>

        <?php
        
     }

    /**
     * Displays combatant table
     * 
     * @param    array    list of combatants
     */
    public function DisplayCombatantTable($combatants, $inInitiative = true, $onDelay = true){

        $count = 0;

        ?>
        <table class="table combat-table">

            <thead>
                <th>
                    Name
                </th>
                
                <th>
                    Init
                </th>
                
                <th>
                    AC
                </th>
                
                <th>
                    HP
                </th>
                
                <th>
                    Saves
                </th>
                
                <th>
                    Speed
                </th>
                
                <th>
                    Attacks
                </th>
            </thead>
        
            <?php foreach($combatants as $combatant): ?>
        
                <tr style="border-bottom: none;">
                    <td style="border-bottom: none;">
                        <div class="combatant-name combatant-name-large">
        
                            <?php 
        
                                $names = explode(' ', $combatant->Name());
        
                                foreach($names as $name):
        
                                    $name   = strtoupper($name);
                                    $length = strlen($name);
                                    $parts  = array();
                                    $num    = 1;
        
                                    $parts[0] = substr($name, 0, $num);
                                    $parts[1] = substr($name, $num, $length );
        
                                    echo '
                                    <span class="combatant-name-first-letter">' . $parts[0] . '</span>
                                    <span class="combatant-name-remainder">' . $parts[1] . '</span>';
        
                                endforeach;
        
                            ?>
                        </div>
                    </td>
        
                    <td style="border-bottom: none;">
                        <?php echo $combatant->InitBonus(); ?>
                    </td>
        
                    <td style="border-bottom: none;">
                        <?php echo $combatant->ArmorClass(); ?>
                    </td>
        
                    <td style="border-bottom: none;">
                        <?php echo $combatant->HitPointsString(); ?>
                    </td>
        
                    <td style="border-bottom: none;">
                        <?php echo $combatant->SaveBonusesString(); ?>
                    </td>
        
                    <td style="border-bottom: none;">
                        <?php echo $combatant->SpeedString(); ?>
                    </td>
        
                    <td style="border-bottom: none;">
                        <?php echo $combatant->GetAttackModesString(); ?>
                    </td>
                </tr>
                
                <tr style="">
                    <td colspan="8" style="">
                        <span class="btn btn-default view-combatant" data-combatant="<?php echo $combatant->Name(); ?>">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </span>
                        
                        <?php
                        
                        if($count == 0):
                        
                            if($inInitiative):
                            ?>
                                <span class="btn btn-default end-turn">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </span>
                                
                                <span class="btn btn-default delay-combatant" data-combatant="<?php echo $combatant->Name(); ?>">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            <?php
                            endif;
                                                
                        endif;
                        
                        ?>
                        
                        <span>
                            <span>
                                Take Damage: <input type="number" value="" class="form-control damage-amount" step="1" style="width: 60px; display: inline;" />
                            </span>

                            <span class="btn btn-default damage-combatant" data-combatant="<?php echo $combatant->Name(); ?>">
                                <span class="glyphicon glyphicon-tint"></span>
                            </span>
                        </span>
                        
                        <?php
                        
                        if($inInitiative):
                        
                        ?>
                        
                        <span class="btn btn-default remove-combatant" data-combatant="<?php echo $combatant->Name(); ?>">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                        
                        <?php
                        else:
                        ?>
                        
                        <span class="btn btn-default add-combatant-to-initiative" data-combatant="<?php echo $combatant->Name(); ?>">
                            <span class="glyphicon glyphicon-plus"></span>
                        </span>
                        
                        <?php
                        endif;
                        ?>
                        
                        <input type="number" class="knowledge-roll form-control" style="width: 60px; display: inline;" value="" placeholder="20" />

                        <span class="btn btn-default make-knowledge-roll" data-combatant="<?php echo $combatant->Name(); ?>">
                            <span class="glyphicon glyphicon-question-sign"></span> <?php echo $combatant->KnowledgeRollSkill(); ?>
                        </span>

                        <span>
                            <?php if($combatant->KnowledgeRollMade()):
                                    if($combatant->GotName()):
                                        echo "Name <em>known</em>.";
                                    else:
                                        echo "Name <em>unknown</em>.";
                                    endif;

                                    if($combatant->Questions() > 0):
                                        echo ' ' . $combatant->Questions() . ' question' . ($combatant->Questions() == 1 ? '' : 's') . '.';
                                    else:
                                        echo ' No questions.';
                                    endif;
                                 endif; ?>
                        </span>
                        
                        
                    </td>
                </tr>
        
            <?php $count++; endforeach; ?>
        </table>
        <?php
    }
    
    /**
     * Displays all pertinent round timers
     */
    public function DisplayRoundTimers(){
        
        if($this->TimersInRound($this->round) > 0){
            
            ?>
            <div class="alert alert-info">
                <strong>Round Triggers!</strong>
                <ul>
                <?php
                
                foreach($this->roundTimer as $timer){
                    if($timer['round_trigger'] == $this->round){
                        echo "<li>{$timer['details']}</li>";
                    }
                }
                
                ?>
                </ul>
            </div>
            <?php
        }
        
    }
    
    /**
     * How many triggers are there this round?
     * 
     * @param   int   the round that we want to check
     * @return  int   how many triggers are this round
     */
    private function TimersInRound($round){
        
        $count = 0;
        
        foreach($this->roundTimer as $t){
            if($t['round_trigger'] == $round){
                $count ++;
            }
        }
        
        return $count;
    }
     
    /**
     * Displays a combatant
     * 
     * @param    str    combatant name
     */
    public function DisplayCombatant($combatantName){
        
        $combatant = null;
        
        $allCombatants = array_merge($this->combatants, $this->combatantsOnDelay, $this->combatantsRemoved);
        
        foreach($allCombatants as $c){
            if($c->Name() == $combatantName){
                $combatant = $c;
                break;
            }
        }
        
        include(ROOT . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'combatant' . DIRECTORY_SEPARATOR . 'view.php');
        
    }
    
    /**
     * Adds a timer (alert) that displays on a round trigger
     * 
     * @param    str    Details
     * @param    int    Which round does this pop up on?
     */
    public function AddRoundTimer($details, $roundTrigger){
        $this->roundTimer[] = array('round_trigger' => $roundTrigger, 'details' => $details);
    }
    
    /**
     * Makes a knowledge roll against a specific combatant in order to know how many questions the character gets
     * 
     * @param   str   Name of the combatant to make the knowledge roll against
     * @param   int   The roll that was made (INCLUDING BONUSES)
     * 
     * @return  int  The number of questions tohe user gets
     */
    public function MakeKnowledgeRoll($combatantBeingInspectedName, $knowledgeRoll) {

        $combatant = null;

        foreach($this->combatants as $c){            
            if($c->Name() == $combatantBeingInspectedName){
                $combatant = $c;
                break;
            }
        }

        if(is_null($combatant)){
            foreach($this->combatantsOnDelay as $c){
                if($c->Name() == $combatantBeingInspectedName){
                    $combatant = $c;
                    break;
                }
            }
        }
        
        if(is_null($combatant)){
            foreach($this->combatantsRemoved as $c){
                if($c->Name() == $combatantBeingInspectedName){
                    $combatant = $c;
                    break;
                }
            }
        }

        if(is_null($combatant)){
            return -1;
        }

        return $c->MakeKnowledgeRoll($knowledgeRoll);
    }

    /**
     * Adds item to combat log
     * 
     * @param    str    Message text
     * @param    str    [INFO, ATTACK, DEATH, ROUND_END, ROUND_START]
     */
    public function Log($message, $type = 'INFO'){
        $this->log[$this->round][] = array('message' => $message, 'type' => $type);
    }

    /**
     * Logs all timer triggers for this round
     */
    public function LogTimers(){
        foreach($this->roundTimer as $t){
            if($t['round_trigger'] == $this->round){
                $this->log($t['details']);
            }
        }
    }
}

?>