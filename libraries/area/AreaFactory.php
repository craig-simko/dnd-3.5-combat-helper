<?php

class AreaFactory{
           
    /**
     * This function takes all the data necessary for an area and serializes it into a json string.
     */     
    public static function SerializeAreaData( $areaName, $dmText, $encounters, $sidebars = array() ){

        $areaData = array(
        
            'areaName'   => $areaName,
            'dmText'     => $dmText,
            'encounters' => $encounters,
            'siidebars'  => $sidebars    
        
        );
        
       $encodedData = json_encode($areaData);
              
       file_put_contents(ROOT . DIRECTORY_SEPARATOR . 'areas' . DIRECTORY_SEPARATOR . $areaName . '.json', $encodedData);
        
    }
    
    /**
     * Returns an area object from its name
     * 
     * @param    string      areaname    The name of the area file WITHOUT the .json extension
     * 
     * @return   obj         area
     */
    public static function CreateAreaByName( $areaName ){
        
        $data = file_get_contents(ROOT . DIRECTORY_SEPARATOR . 'areas' . DIRECTORY_SEPARATOR . $areaName . '.json');
        
        $area = AreaFactory::DeserializeAreaData( $data );
                
        return $area;        
    }
    
    /**
     * Returns an Area Object from an array of data
     * 
     * @param    array|string    An array from the json_decoded string created by SerializeAreaData. OR json_string created by SerializeAreaData
     * 
     * @return   obj      Combatant()
     */
    public static function DeserializeAreaData( $data ){
        
        if(!is_array($data)){
            $data = json_decode($data, true);
        }
        
        $areaName   = $data['areaName'];
        $dmText     = $data['dmText'];
        $encounters = $data['encounters'];
        $sidebars   = $data['siidebars'];
        
        $area = new Area( $areaName, $dmText, $encounters, $sidebars );
        
        return $area;
    }
    
    /**
     * Returns an array of names of areas
     * 
     * @return    array    Array of area names
     */
    public static function GetAreaList(){

        $areaFiles = scandir(ROOT . DIRECTORY_SEPARATOR . 'areas');

        $areaNames = array();

        foreach($areaFiles as $af){
            if($af == '.' || $af == '..'){
                continue;
            }

            $af = str_replace('.json', '', $af);

            $areaNames[] = $af;
        }

        return $areaNames;
    }
}

?>