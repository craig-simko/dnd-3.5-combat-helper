<?php

class Area{
    
    private $areaName   = '';
    private $dmText     = '';
    private $encounters = null;
    private $sidebars   = array();
        
    /**
     * Instantiates the encounter
     * 
     * @param    string      areaName          Name of the encounter. usually Dungeon Name - A#. Encounter Name
     * @param    string      dmText            What really goes on behind the scenes in this room. For DM eyes only
     * @param    array       encounters        List of encounters in the area
     * @param    array       sidebars          Array of sidebars for the area
     */
    public function __construct( $areaName, $dmText, $encounters, $sidebars = array() ){

        $this->areaName   = $areaName;
        $this->dmText     = $dmText;
        $this->encounters = $encounters;                
        $this->sidebars   = $sidebars;
        
        if(!is_array($this->encounters)){
            $this->encounters = array();
        }
        
        natcasesort($this->encounters);        
    }
    
    public function AreaName(){
        return $this->areaName;
    }
    
    public function DmText(){
        return $this->dmText;
    }
        
    /**
     * Returns an array of Combatant objects freshly created
     * 
     * @return    array    Combatants in this encounter
     */
    public function Encounters(){

        $encounters = array();

        foreach($this->encounters as $e){            

            $encounters[] = EncounterFactory::CreateEncounterByName($e);

        }

        return $encounters;
    }
    
    public function GetEncounterList(){
        return $this->encounters;
    }
    
    public function SideBars(){
        return $this->sidebars;
    }
}

?>