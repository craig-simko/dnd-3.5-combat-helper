<?php

class Encounter{

    private $encounterName = '';
    private $boxText       = '';
    private $dmText        = '';
    private $creatures     = null;
    private $combatants    = array();
    private $treasure      = null;
    private $traps         = null;

    /**
     * Instantiates the encounter
     * 
     * @param    string      encounterName     Name of the encounter. usually Dungeon Name - A#. Encounter Name
     * @param    string      boxText           What to read to the PCs when they enter the room
     * @param    string      dmText            What really goes on behind the scenes in this room. For DM eyes only
     * @param    string      creatures         Text about what creatures live in here and do in here and what not.
     * @param    array       combatants        An array of strings that are the names of combatants that exist in this room.
     * @param    string      treasure          What treasure lies in here?
     * @param    string      traps             What traps are in this room, their DCs and triggers etc.
     */
    public function __construct( $encounterName, $boxText, $dmText, $creatures = null, $combatants = array(), $treasure = null, $traps = null ){

        $this->encounterName = $encounterName;
        $this->boxText       = $boxText;
        $this->dmText        = $dmText;
        $this->creatures     = $creatures;
        $this->combatants    = $combatants;
        $this->treasure      = $treasure;
        $this->traps         = $traps;

        $pcs = array();
        $pcFiles = scandir('pcs');

        foreach($pcFiles as $pcFile){
            if($pcFile == '.' || $pcFile == '..'){
                continue;
            }

            $pcs[] = array(
                            'name'  => str_replace('.json', '', $pcFile),
                            'count' => 1,
                            'pc'    => true
                        );
        }
        
        $this->combatants = array_merge($this->combatants, $pcs);
        
    }
    
    private function StripWordQuotations($text){        
        $text = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
        return str_replace("\r\n", ' ', $text);  
    }
    
    public function EncounterName(){
        return $this->StripWordQuotations($this->encounterName);
    }
    
    public function BoxText(){
        return $this->StripWordQuotations($this->boxText);
    }
    
    public function DmText(){        
        return $this->StripWordQuotations($this->dmText);
    }
    
    public function Creatures(){
        return $this->StripWordQuotations($this->creatures);
    }
    
    /**
     * Returns an array of Combatant objects freshly created
     * 
     * @return    array    Combatants in this encounter
     */
    public function Combatants(){

        $combatants = array();

        foreach($this->combatants as $c){    
            $count = intval($c['count']);
            
            $rename = $count > 1;
                
            for($i = 1; $i <= $count; $i++){
                
                if( isset($c['pc']) && $c['pc'] ){
                    $combatants[] = CombatantFactory::CreatePcCombatantByName($c['name'], ($rename ? $i : null));
                }else{
                    $combatants[] = CombatantFactory::CreateCombatantByName($c['name'], ($rename ? $i : null));
                }
            }
            
        }

        return $combatants;
    }
    
    /**
     * Returns the number of combatants in this encounter
     */
    public function CombatantCount(){
        return count($this->combatants);
    }
    
    public function Treasure(){
        return $this->StripWordQuotations($this->treasure);
    }
    
    public function Traps(){
        return $this->StripWordQuotations($this->traps);
    }
}

?>