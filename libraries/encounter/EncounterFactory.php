<?php

class EncounterFactory{
           
    /**
     * This function takes all the data necessary for an encounter and serializes it into a json string.
     */     
    public static function SerializeEncounterData(  $encounterName, $boxText, $dmText, $creatures = null, $combatants = array(), $treasure = null, $traps = null ){

        $encounterData = array(
        
            'encounterName' => $encounterName,
            'boxText'       => $boxText,
            'dmText'        => $dmText,
            'creatures'     => $creatures,
            'combatants'    => $combatants,
            'treasure'      => $treasure,
            'traps'         => $traps            
        
        );
        
       $encodedData = json_encode($encounterData);
              
       file_put_contents(ROOT . DIRECTORY_SEPARATOR . 'encounters' . DIRECTORY_SEPARATOR . $encounterName . '.json', $encodedData);
        
    }
    
    /**
     * Returns a combatant object from its name
     * 
     * @param    string      combatantName    The name of the combatant file WITHOUT the .json extension
     * @param    int|null    renameCount      If we want to rename a combatant OBJECT then send the count here.
     * 
     * @return   obj       combatant
     */
    public static function CreateEncounterByName( $encounterName ){
        
        $data = file_get_contents(ROOT . DIRECTORY_SEPARATOR . 'encounters' . DIRECTORY_SEPARATOR . $encounterName . '.json');
        
        $encounter = EncounterFactory::DeserializeEncounterData( $data );
                
        return $encounter;        
    }
    
    /**
     * Returns an Encounter Object from an array of data
     * 
     * @param    array|string    An array from the json_decoded string created by SerializeEncounterData. OR json_string created by SerializeEncounterData
     * 
     * @return   obj      Combatant()
     */
    public static function DeserializeEncounterData( $data ){
        
        if(!is_array($data)){
            $data = json_decode($data, true);
        }
        
        $encounterName = $data['encounterName'];
        $boxText       = $data['boxText'];
        $dmText        = $data['dmText'];
        $creatures     = $data['creatures'];
        $combatants    = $data['combatants'];
        $treasure      = $data['treasure'];
        $traps         = $data['traps'];
        
        $encounter = new Encounter($encounterName, $boxText, $dmText, $creatures, $combatants, $treasure, $traps);
        
        return $encounter;
    }
    
    /**
     * Returns an array of names of encounters
     * 
     * @return    array    Array of encounter names
     */
    public static function GetEncounterList(){

        $encounterFiles = scandir(ROOT . DIRECTORY_SEPARATOR . 'encounters');
        
        natcasesort($encounterFiles);

        $encounterNames = array();

        foreach($encounterFiles as $ef){
            if($ef == '.' || $ef == '..'){
                continue;
            }

            $ef = str_replace('.json', '', $ef);

            $encounterNames[] = $ef;
        }

        return $encounterNames;
    }
}

?>