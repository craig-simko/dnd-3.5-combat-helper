function IssueCombatCommand(command, data){
    $.post( "libraries/combat/AjaxCombatCommands.php", {
        'command' : command,
        'data'    : data
    }, function( data ) {
        $('#combat-container').html(data);
    });
}

function GetCombatantFullView(combatantIndex){
    $.post( "libraries/combat/AjaxCombatCommands.php", {
        'command' : 'GetCombatantView',
        'data'    : combatantIndex
    }, function( data ) {            
        $('.combatant-view-container').fadeOut('1');
        
        $(".combatant-view-container").html(data);
        
        $('.combatant-view-container').fadeIn('1');
    });
}

$(document).ready(function(){
   
    jQuery(document.body).on('click', '.start-combat', function(event) {
        
        inits = {};
        
        $(".init-combatant").each(function(){
            var combatant = $(this).data('combatant');
            var init      = $(this).val();
            
            inits[combatant] = init;
        });
        
        inits = JSON.stringify(inits);
        
        var command = 'RollInitiative';
        
        IssueCombatCommand(command, inits);
        
    });
    
    // end-turn
    jQuery(document.body).on('click', '.end-turn', function(event) {
       
       IssueCombatCommand('EndTurn', '');
        
    });
    
    // View Combatant
    jQuery(document.body).on('click', '.view-combatant', function(event) {
       
       var combatantIndex = $(this).data('combatant');
       
       combatantIndex = JSON.stringify(combatantIndex);      
       
       GetCombatantFullView(combatantIndex);
    });
    
    // damage combatant
    jQuery(document.body).on('click', '.damage-combatant', function(event) {
       
       var data = {};
       
       var combatantIndex = $(this).data('combatant');
       var amount         = $(this).parent().find('.damage-amount').val();
       
       data['combatant_name'] = combatantIndex;
       data['damage_amount']  = amount;
       
       data = JSON.stringify(data);   
              
       IssueCombatCommand('TakeDamage', data);
    });
    
    // delay combatant
    jQuery(document.body).on('click', '.delay-combatant', function(event) {
       
       var data = {};
       
       var combatantName = $(this).data('combatant');
       
       data['combatant_name'] = combatantName;
       
       data = JSON.stringify(data);   
              
       IssueCombatCommand('DelayCombatant', data);
    });
    
    // remove combatant from initiative order
    jQuery(document.body).on('click', '.remove-combatant', function(event) {
       
       var data = {};
       
       var combatantName = $(this).data('combatant');
       
       data['combatant_name'] = combatantName;
       
       data = JSON.stringify(data);   
              
       IssueCombatCommand('RemoveCombatant', data);
    });
    
    // Add combatant back into initiaive order
    jQuery(document.body).on('click', '.add-combatant-to-initiative', function(event) {
       
       var data = {};
       
       var combatantName = $(this).data('combatant');
       
       data['combatant_name'] = combatantName;
       
       data = JSON.stringify(data);   
              
       IssueCombatCommand('AddCombatantBackToInitiativeOrder', data);
    });
    
    // Add a NEW combatant into initiative order
    // add-combatant
    jQuery(document.body).on('click', '.add-combatant', function(event) {
       
       var data = {};
       
       var combatantName = $(this).parent().parent().parent().parent().find('.combatant-add-select').val();
       var amount        = $(this).parent().parent().parent().parent().find('.combatant-add-amount').val();
              
       data['combatant_name'] = combatantName;
       data['count']          = amount;
       
       data = JSON.stringify(data);   
              
       IssueCombatCommand('AddNewCombatant', data);
    });
    
    // Add round timer
    jQuery(document.body).on('click', '.round-timer-add', function(event) {
        
        var roundTrigger = $(".round-timer-round-trigger").val();
        var details      = $(".round-timer-details").val();
        
        roundTrigger = parseInt(roundTrigger);
        
        if(roundTrigger < 0 || details == ''){
            return;
        }
        
        var command = 'AddRoundTimer';
        
        var data = {};
        
        data['round_trigger'] = roundTrigger;
        data['details']       = details;
        
        data = JSON.stringify(data);  
        
        IssueCombatCommand(command, data);
        
    });
    
    // makes a knowledge check
    jQuery(document.body).on('click', '.make-knowledge-roll', function(event) {

        var combatantName = $(this).data('combatant');
        var knowledgeRoll = $(this).parent().find('.knowledge-roll').val();

        var command = 'MakeKnowledgeRoll';
        var data    = {};

        data['combatant_name'] = combatantName;
        data['knowledge_roll'] = knowledgeRoll;
        
        data = JSON.stringify(data);

        IssueCombatCommand(command, data);

    });
});