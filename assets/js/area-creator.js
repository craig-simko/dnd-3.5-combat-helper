var sidebarCount = 0;

$(document).ready(function(){
    
    // Add a Sidebar
    $('.add-area-sidebar').click(function(){
        
        sidebarCount++;
                
        $.post( "views/area_creator/sidebar.php", {
            
            'sidebar_count' : sidebarCount
            
        }, function( data ) {
            
            $( "#sidebar-super-container" ).append( '<div class="sidebar-container">' + data + '</div>' );
        });

    });
    
    // Removes a Sidebar
    jQuery(document.body).on('click', '.remove-sidebar', function(event) {
       
       $(this).parent().parent().parent().remove();
        
    });
    
});