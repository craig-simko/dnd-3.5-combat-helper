var combatantCount = 0;

$(document).ready(function(){
    
    // Add a Combatant
    $('.add-combatant').click(function(){
        
        combatantCount++;
                
        $.post( "views/encounter_creator/combatant.php", {
            
            'combatant_count' : combatantCount
            
        }, function( data ) {
            
            $( "#combatant-super-container" ).append( '<div class="combatant-container">' + data + '</div>' );
        });

    });
    
    // Removes an attack mode
    jQuery(document.body).on('click', '.remove-combatant', function(event) {
       //combatantCount--;
              
       //$(this).parent().parent().parent().parent().slideUp();
       $(this).parent().parent().parent().parent().remove();
        
    });
    
});