$(document).ready(function(){
    
    // Change Area
    $('#active-area-selector').change(function(){
        
        var areaName = $(this).val();
        
        if(areaName == ''){
            return;
        }
                        
        window.location.href = "index.php?a=adventure&area=" + areaName;

    });    
    
    // Change Encounter
    $("#active-encounter-selector").change(function(){
        
        var areaName      = $('#active-area-selector').val();
        var encounterName = $(this).val();
        
        if(encounterName == ''){
            return;
        }

        console.log(encounterName);

        window.location.href = "index.php?a=adventure&area=" + areaName + "&encounter=" + encounterName;

    });
    
});