var sizes = {
            FINE       : 8,
            DIMINUTIVE : 4,
            TINY       : 2,
            SMALL      : 1,
            MEDIUM     : 0,
            LARGE      : -1,
            HUGE       : -2,
            GARGANTUAN : -4,
            COLOSSAL   : -8
        };
// #size-category        #armor-class-size
var attackModeCount       = 0;
var attackCount           = 0;
var damageCount           = 0;
var damageReductionCount  = 0;
var damageResistanceCount = 0;
var specialAbilityCount   = 0;
var skillCount            = 0;

function getAttrMod(attr){
    
    if(attr == 'NONE'){
        return 0;
    }
    
    var attrVal = $("#" + attr.toLowerCase() + "-input").val();
    
    if(attrVal == ''){
        return 0;
    }
    
    attrVal = parseInt(attrVal);
    
    if(attrVal > 10){
        return parseInt( ( attrVal - 10 ) / 2 );    
    }else{
        var mod = attrVal - 10;
        mod = mod / 2;
        mod = Math.floor(mod);
        
        return mod;
    }
    
    
}

function updateAttacks(){
    
    var size_category = $("#size-category").val();
    var size_modifier = sizes[size_category];
        
    $(".attack-bonus-size-mod").each(function(){
        $(this).data('size-modifier', size_modifier);
        $(this).html(size_modifier + ' (' + size_category + ')');
    });
    
    $(".attack-bonus-container").each(function(){
        var bab          = parseInt( $(this).find('.base-attack-bonus').val() );
        var extraBonus   = parseInt( $(this).find('.attack-mode-bonus').val() );
        
        attrModTotal = 0;
        attrMods     = '';
        attrs        = new Array();
        
        $('option:selected', $(this).find('.attack-attrs')).each(function(){
            var attr = $(this).val();
            
            attrs.push(attr);
        });

        for(var i = 0; i < attrs.length; i++){
            
            var mod = getAttrMod(attrs[i])
            
            attrModTotal += mod;
            attrMods += mod + " (" + attrs[i] + ")" + '<br />';
        }
        
        var total = bab + extraBonus + attrModTotal + size_modifier;
        
        $(this).find('.attack-bonus-total').html(total);
        
        $(this).find('.attack-attr-mod').html(attrMods);
        
        //console.log(bab);
        //console.log(extraBonus);
        //console.log(size_modifier);
        
        //console.log(total);
    });
    
    updateDamages();
}

function updateDamages(){
    
    $(".damage-container").each(function(){
        
        var damage_die_count       = $(this).find('.damage-die-count').val();
        var damage_die_type        = $(this).find('.damage-die-type').val();
        var damage_bonus           = $(this).find('.damage-bonus').val();
        var damage_attr_multiplier = $(this).find('.damage-attr-multiplier').val(); 
        
        damageModTotal = 0;
        damageMods     = '';
        attrs          = new Array();
        
        $('option:selected', $(this).find('.damage-attrs')).each(function(){
            var attr = $(this).val();
            
            attrs.push(attr);
        });
        
        for(var i = 0; i < attrs.length; i++){
            
            var mod = getAttrMod(attrs[i]);
            
            damageModTotal += parseInt( mod * damage_attr_multiplier );
            damageMods     += parseInt( mod * damage_attr_multiplier ) + " (" + attrs[i] + ")" + '<br />';
        }
        
        damageModTotal = parseInt(damageModTotal) + parseInt(damage_bonus);
        
        var damage_string = damage_die_count + "d" + damage_die_type + "+" + damageModTotal;

        $(this).find('.damage-attr-mod').html(damageMods);
        $(this).find('.damage-total').html(damage_string);
        
        //console.log(damage_string);
    });
}

function calculateMaxHitPoints(){
    
    var baseHitPoints = parseInt( $("#base-hit-points-input").val() );
    var conMod        = getAttrMod('con'); //parseInt( ( parseInt( $("#con-input").val() ) - 10 ) / 2 );
    var hitDice       = parseInt( $("#hit-dice-input").val() );
    
    var maxHitPoints  = parseInt( baseHitPoints + ( conMod * hitDice ) );
    
    $("#hit-point-container").html(maxHitPoints);
    
}

function calculateSaveTotal(type){
        
    switch(type){
        case('REF'):
            var baseSave  = parseInt( $('#saveBonusRefInput').val() );
            var mod       = getAttrMod('dex'); //parseInt( ( parseInt( $('#dex-input').val() ) - 10 ) / 2 );
            var totalSpan = $('#saveRefTotal');
        break;
        
        case('FORT'):
            var baseSave  = parseInt( $('#saveBonusFortInput').val() );
            var mod       = getAttrMod('con'); //parseInt( ( parseInt( $('#con-input').val() ) - 10 ) / 2 );
            var totalSpan = $('#saveFortTotal');
        break;
        
        case('WILL'):
            var baseSave  = parseInt( $('#saveBonusWillInput').val() );
            var mod       = getAttrMod('wis'); //parseInt( ( parseInt( $('#wis-input').val() ) - 10 ) / 2 );
            var totalSpan = $('#saveWillTotal');
        break;
    }
        
    totalSpan.html(baseSave + mod);
    
}

function calculateSkillTotal(container){
    
    var totalSpan    = container.find('.skill-total');
    var skillModSpan = container.find('.skill-attr-mod');
    var bonus        = container.find('.skill-bonus').val();
    
    var select       = container.find('.skill-select');
    
    var modAttr = '';
    
    modAttr = select.find('option:selected').data('attr');
    
    mod = getAttrMod(modAttr.toLowerCase()); //parseInt( ( parseInt( $("#" + ( modAttr.toLowerCase() ) + "-input").val() ) - 10 ) / 2 );
    
    skillModSpan.html(mod + ' ' + modAttr);
    
    totalSpan.html( parseInt(bonus) + parseInt(mod));
}

$(document).ready(function(){
    
    $(".attr-input").keyup(function(){
        $(".skill-container").each(function(){
            calculateSkillTotal($(this));
        });
        
        updateAttacks();
        updateDamages();
    });
    
    $(".attr-input").click(function(){
        $(".skill-container").each(function(){
            calculateSkillTotal($(this));
        });
        
        updateAttacks();
        updateDamages();
    });
    
    // UPDATE ATTACKS
    jQuery(document.body).on('keyup', '.base-attack-bonus', function(){
        updateAttacks();
    });
    
    jQuery(document.body).on('click', '.base-attack-bonus', function(){
        updateAttacks();
    });
    
    jQuery(document.body).on('keyup', '.attack-mode-bonus', function(){
        updateAttacks();
    });
    
    jQuery(document.body).on('click', '.attack-mode-bonus', function(){
        updateAttacks();
    });
    
    jQuery(document.body).on('change', '.attack-attrs', function(){
       updateAttacks(); 
    });
    
    // UPDATE DAMAGES
    jQuery(document.body).on('keyup', '.damage-input', function(){
        updateDamages();
    });
    
    jQuery(document.body).on('click', '.damage-input', function(){
        updateDamages();
    });
    
    jQuery(document.body).on('change', '.damage-input', function(){
        updateDamages();
    });
    
    // SKILL TOTALS
    jQuery(document.body).on('keyup', '.skill-bonus', function(){
        calculateSkillTotal($(this).parent());
    });
    
    jQuery(document.body).on('click', '.skill-bonus', function(){
        calculateSkillTotal($(this).parent());
    });

    // Update max hit point span
    $('#base-hit-points-input').keyup(function(){
       calculateMaxHitPoints(); 
    });
    
    $('#base-hit-points-input').click(function(){
       calculateMaxHitPoints(); 
    });
    
    $('#con-input').keyup(function(){
       calculateMaxHitPoints(); 
    });
    
    $('#con-input').click(function(){
       calculateMaxHitPoints(); 
    });
    
    $('#hit-dice-input').keyup(function(){
       calculateMaxHitPoints(); 
    });
    
    $('#hit-dice-input').click(function(){
       calculateMaxHitPoints(); 
    });
    
    // SAVE UPDATERS
    $('#saveBonusRefInput').click(function(){
        calculateSaveTotal('REF');
    });
    
    $('#saveBonusRefInput').keyup(function(){
        calculateSaveTotal('REF');
    });
    
    $('#dex-input').keyup(function(){
       calculateSaveTotal('REF');
    });
    
    $('#dex-input').click(function(){
       calculateSaveTotal('REF');
    });
    
    $('#saveBonusFortInput').click(function(){
        calculateSaveTotal('FORT');
    });
    
    $('#saveBonusFortInput').keyup(function(){
        calculateSaveTotal('FORT');
    });
    
    $('#con-input').keyup(function(){
       calculateSaveTotal('FORT');
    });
    
    $('#con-input').click(function(){
       calculateSaveTotal('FORT');
    });
    
    $('#saveBonusWillInput').click(function(){
        calculateSaveTotal('WILL');
    });
    
    $('#saveBonusWillInput').keyup(function(){
        calculateSaveTotal('WILL');
    });
    
    $('#wis-input').keyup(function(){
       calculateSaveTotal('WILL');
    });
    
    $('#wis-input').click(function(){
       calculateSaveTotal('WILL');
    });
    
    // Update armor class based on size
    $('#size-category').change(function(){
        var size_category = $(this).val();
        
        var size_modifier = sizes[size_category];
        
        $("#armor-class-size").val(size_modifier);
        
        updateAttacks();
    })
   
    // Add an attack mode
    $('.add-attack-mode').click(function(){
        
        attackModeCount++;
        
        var c = 1;

        $('.attack-mode-count').each(function(index, element){
            
            //console.log(index);
            //console.log(element);
            
            if($(this).is(':visible')){
                $(this).html(c);
                c++;
            }
        });
        
        $.post( "views/combatant_creator/attack_mode.php", {
            
            'attack_mode_count' : attackModeCount,
            'count_display'     : c
            
        }, function( data ) {
            
            $( "#attack-mode-super-container" ).append( '<div class="attack-mode-container" data-attack-mode-count="' + attackModeCount + '">' + data + '</div>' );
        });
        
        

    });
    
    // Removes an attack mode
    jQuery(document.body).on('click', '.remove-attack-mode', function(event) {
       //attackModeCount--;
              
       //$(this).parent().parent().parent().parent().slideUp();
       $(this).parent().parent().parent().parent().remove();
        
       var c = 1;
       $('.attack-mode-count').each(function(){
           if($(this).is(':visible')){
               $(this).html(c);
               c++;
           }            
       });

    });
    
    // Add attack
    jQuery(document.body).on('click', '.add-attack', function(event) {
              
       var ele = $(this);
       
       attackMode = ele.parent().parent().parent().find( ".attack-super-container" ).data('attack-mode-count');
       
       attackCount++;
              
       $.post( "views/combatant_creator/attack.php", {
        
            'attack_mode_count' : attackMode,
            'attack_count'      : attackCount
        
       }, function( data ) {
                
            ele.parent().parent().parent().find( ".attack-super-container" ).append( '<div class="attack-container" data-attack-count="' + attackCount + '"><hr /><b>Attack</b>: ' + data + '<hr /></div>' );
            
            updateAttacks();
        });

    });
    
    // Remove attack
    jQuery(document.body).on('click', '.remove-attack', function(event) {
              
       $(this).parent().remove();

    });
    
    // Add damage
    jQuery(document.body).on('click', '.add-damage', function(){
        
        var ele = $(this);
        
        damageCount++;
        
        var attackMode  = ele.parent().parent().parent().find( ".attack-super-container" ).data('attack-mode-count');
        var attackCount = ele.data('attack-count');

        $.post( "views/combatant_creator/damage.php", {
           
           'attack_mode_count' : attackMode,
           'attack_count'      : attackCount,
           'damage_count'      : damageCount
            
        }, function( data ) {            
            ele.parent().find( ".damage-super-container" ).append( '<div class="damage-container">' + data + '</div>' );
        });

    });
    
    // Remove damage
    jQuery(document.body).on('click', '.remove-damage', function(){
        
        $(this).parent().parent().parent().parent().remove();

    });
    
    // Add damage reduction
    jQuery(document.body).on('click', '.add-damage-reduction', function(){
        
        var ele = $(this);
        
        damageReductionCount++;
                
        $.post( "views/combatant_creator/damage_reduction.php", {
            'damage_reduction_count' : damageReductionCount
        }, function( data ) {            
            ele.parent().parent().find( ".damage-reduction-super-container" ).append( '<div class="damage-reduction-container">' + data + '</div>' );
        });

    });
    
    // Remove damage reduction
    jQuery(document.body).on('click', '.remove-damage-reduction', function(){

        $(this).parent().parent().parent().remove();

    });

    // Add damage Resistance
    jQuery(document.body).on('click', '.add-damage-resistance', function(){

        var ele = $(this);

        damageResistanceCount++;

        $.post( "views/combatant_creator/damage_resistance.php", {
            'damage_resistance_count' : damageResistanceCount
        }, function( data ) {            
            ele.parent().parent().find( ".damage-resistance-super-container" ).append( '<div class="damage-resistance-container">' + data + '</div>' );
        });

    });

    // Remove damage Resistance
    jQuery(document.body).on('click', '.remove-damage-resistance', function(){
        
        $(this).parent().parent().parent().remove();

    });
    
    // Add Special Ability
    jQuery(document.body).on('click', '.add-special-ability', function(){
        
        var ele = $(this);
        
        specialAbilityCount++;
                
        $.post( "views/combatant_creator/special_ability.php", {
            'special_ability_count' : specialAbilityCount
        }, function( data ) {            
            ele.parent().parent().find( ".special-ability-super-container" ).append( '<div class="special-ability-container">' + data + '</div>' );
        });

    });
    
    // Remove Special Ability
    jQuery(document.body).on('click', '.remove-special-ability', function(){
        
        $(this).parent().parent().parent().remove();

    });
    
    // Add Skill
    jQuery(document.body).on('click', '.add-skill', function(){
        
        var ele = $(this);
        
        skillCount++;
                
        $.post( "views/combatant_creator/skill.php", {
            'skill_count' : skillCount
        }, function( data ) {            
            ele.parent().parent().find( ".skill-super-container" ).append( '<div class="skill-container">' + data + '</div>' );
        });
        
        $('.skill-container').each(function(){
            calculateSkillTotal($(this));
        })
    });
    
    // Remove Skill
    jQuery(document.body).on('click', '.remove-skill', function(){
        
        $(this).parent().parent().parent().remove();

    });
    
    // Skill select
    jQuery(document.body).on('change', '.skill-select', function(){
       calculateSkillTotal($(this).parent());
    });
    
});