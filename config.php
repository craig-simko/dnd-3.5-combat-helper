<?php

define('ROOT', __DIR__);

$ATTRIBUTES = array('STR', 'DEX', 'CON', 'INT', 'WIS', 'CHA');

$ALIGNMENTS_LAW_VS_CHAOS = array('LAWFUL', 'NEUTRAL', 'CHAOTIC');
$ALIGNMENTS_GOOD_VS_EVIL = array('GOOD', 'NEUTRAL', 'EVIL');

$DAMAGE_TYPES = array(
                            'BLUDGEONING',
                            'PIERCING',
                            'SLASHING',
                            'PRECISION',
                            'FIRE',
                            'ACID',
                            'COLD',
                            'SONIC',
                            'LIGHTNING',
                            'DIVINE',
                            'FORCE',
                            'UNTYPED'
                        );
                        
$CREATURE_TYPES = array(
                            'ABERRATION',                            
                            'ANIMAL',                            
                            'CONSTRUCT',                            
                            'DRAGON',                            
                            'ELEMENTAL',                            
                            'FEY',                            
                            'GIANT',                            
                            'HUMANOID',                            
                            'MAGICAL BEAST',                            
                            'MONSTROUS HUMANOID',                            
                            'OOZE',                            
                            'OUTSIDER',                            
                            'PLANT',                            
                            'UNDEAD',                            
                            'VERMIN'
                        );
                        
$CREATURE_SUBTYPES = array(
                                'NONE',
                                'AIR',
                                'ANGEL',
                                'AQUATIC',
                                'ARCHON',
                                'AUGMENTED',
                                'CHAOTIC',
                                'COLD',
                                'EARTH',
                                'EVIL',
                                'EXTRAPLANAR',
                                'FIRE',
                                'GOBLINOID',
                                'GOOD',
                                'INCORPOREAL',
                                'LAWFUL',
                                'NATIVE',
                                'SHAPECHANGER',
                                'SWARM',
                                'WATER'                        
                        );
                        
$SIZES = array(
            'FINE'       => 8,
            'DIMINUTIVE' => 4,
            'TINY'       => 2,
            'SMALL'      => 1,
            'MEDIUM'     => 0,
            'LARGE'      => -1,
            'HUGE'       => -2,
            'GARGANTUAN' => -4,
            'COLOSSAL'   => -8
        );

$SPECIAL_SIZE_MODIFIERS = array(
            'FINE'       => -16,
            'DIMINUTIVE' => -12,
            'TINY'       => -8,
            'SMALL'      => -4,
            'MEDIUM'     => 0,
            'LARGE'      => 4,
            'HUGE'       => 8,
            'GARGANTUAN' => 12,
            'COLOSSAL'   => 16
);
        
$SPECIAL_ABILITY_TYPES = array(
            'Ex' => 'Extraordinary',
            'Sp' => 'Spell-Like',
            'Su' => 'Supernatural'
        );
        
$SKILLS = array(
    'Appraise'                                  => 'INT',
    'Balance'                                   => 'DEX',
    'Bluff'                                     => 'CHA',
    'Climb'                                     => 'STR',
    'Concentration'                             => 'CON',
    'Craft'                                     => 'INT',
    'Decipher Script'                           => 'INT',
    'Diplomacy'                                 => 'CHA',
    'Disable Device'                            => 'INT',
    'Disguise'                                  => 'CHA',
    'Escape Artist'                             => 'DEX',
    'Forgery'                                   => 'INT',
    'Gather Information'                        => 'CHA',
    'Handle Animal'                             => 'CHA',
    'Heal'                                      => 'WIS',
    'Hide'                                      => 'DEX',
    'Intimidate'                                => 'CHA',
    'Jump'                                      => 'STR',
    'Knowledge (arcana)'                        => 'INT',
    'Knowledge (architecture and engineering)'  => 'INT',
    'Knowledge (dungeoneering)'                 => 'INT',
    'Knowledge (geography)'                     => 'INT',
    'Knowledge (history)'                       => 'INT',
    'Knowledge (local)'                         => 'INT',
    'Knowledge (nature)'                        => 'INT',
    'Knowledge (nobility and royalty)'          => 'INT',
    'Knowledge (religion)'                      => 'INT',
    'Knowledge (the planes)'                    => 'INT',
    'Listen'                                    => 'WIS',
    'Move Silently'                             => 'DEX',
    'Open Lock'                                 => 'DEX',
    'Perform'                                   => 'CHA',
    'Profession'                                => 'WIS',
    'Ride'                                      => 'DEX',
    'Search'                                    => 'INT',
    'Sense Motive'                              => 'WIS',
    'Sleight of Hand'                           => 'DEX',
    'Spellcraft'                                => 'INT',
    'Spot'                                      => 'WIS',
    'Survival'                                  => 'WIS',
    'Swim'                                      => 'STR',
    'Tumble'                                    => 'DEX',
    'Use Magic Device'                          => 'CHA',
    'Use Rope'                                  => 'DEX'
);

$LANGUAGES = array(
    'Abyssal',
    'Aquan', 
    'Auran',
    'Celestial',
    'Common',
    'Draconic',
    'Druidic',
    'Dwarven',
    'Elven',
    'Giant',
    'Gnome',
    'Goblin',
    'Gnoll',
    'Halfling',
    'Ignan',
    'Infernal',
    'Orc',
    'Sylvan',
    'Terran',
    'Undercommon'
);

?>