<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>DnD 3.5 Helper</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- MY CSS -->
    <link href="assets/css/main.css" rel="stylesheet">
    <link href="assets/css/alert-custom.css" rel="stylesheet">
    
    <link href="assets/css/encounter.css" rel="stylesheet">
    <link href="assets/css/area.css" rel="stylesheet">
    <link href="assets/css/combatant.css" rel="stylesheet">
    <link href="assets/css/combat.css" rel="stylesheet">
    
    <script src="assets/js/bootstrap.min.css"></script>


    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Fondamento" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    
    
    <!-- icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="assets/images/icons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="assets/images/icons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="assets/images/icons/manifest.json">
    <link rel="mask-icon" href="assets/images/icons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/images/icons/favicon.ico">
    <meta name="msapplication-config" content="assets/images/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <script src="assets/js/combatant-creator.js"></script>
    <script src="assets/js/encounter-creator.js"></script>
    <script src="assets/js/area-creator.js"></script>
    <script src="assets/js/combat.js"></script>
    <script src="assets/js/area-selector.js"></script>
    
    <script type="text/javascript">
        var nice = false;
        
        $(document).ready(function() {
            nice = $("html").niceScroll();
        });
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <?php include_once('views/navbar.php'); ?>
  
  <div class="super-container">