<?php 

include_once('../../config.php');
include_once('../../libraries/combatant/CombatantFactory.php');

$combatantCount = $_POST['combatant_count'];

$combatantList = CombatantFactory::GetCombatantList();

?>

<div class="form-group">        
    <label for="inputDamageResistance" class="col-sm-3 control-label">Combatant</label>
    
    <div class="col-sm-9">
        <div class="form-inline">  
                                                   
            <select name="combatants[<?= $combatantCount ?>][name]" class="form-control skill-select">
                <?php foreach($combatantList as $combatantName): ?>
                
                <option value="<?= $combatantName ?>"><?= $combatantName ?></option>
                
                <?php endforeach; ?>
            </select>
            
            x <input name="combatants[<?= $combatantCount ?>][count]" type="number" class="form-control skill-bonus" style="max-width: 65px;" value="1" />
                        
            <div class="btn btn-danger remove-combatant"><span class="glyphicon glyphicon-minus"></span>Remove Combatant</div>
        </div>
    </div>
    
        
</div>