<?php

$combatants = CombatantFactory::GetCombatantList();

if(isset($_POST['encounter-name'])){

    $encounterName = $_POST['encounter-name'];
    $boxText       = $_POST['box-text'];
    $dmText        = $_POST['dm-text'];
    $creatures     = $_POST['creatures'];
    $traps         = $_POST['traps'];
    $treasure      = $_POST['treasure'];
    $combatants    = isset($_POST['combatants']) ? $_POST['combatants'] : array();      
    
    EncounterFactory::SerializeEncounterData($encounterName, $boxText, $dmText, $creatures, $combatants, $treasure, $traps);    
    
    ?>  
        <div class="alert alert-success">
            You have successfully created the encounter: <strong><em><?php echo $encounterName; ?></em></strong>
        </div>
    <?php    
}

?>

<h1>Encounter Creator</h1>

<form class="form-horizontal" method="post">

    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">Encounter Name</label>
        
        <div class="col-sm-10">     
            <input name="encounter-name" class="form-control" />
        </div>
    </div>

    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">Box Text</label>
        
        <div class="col-sm-10">     
            <textarea name="box-text" rows="7" class="form-control"></textarea>
        </div>
    </div>
    
    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">DM Text</label>
        
        <div class="col-sm-10">     
            <textarea name="dm-text" rows="7" class="form-control"></textarea>
        </div>
    </div>
    
    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">Creatures</label>
        
        <div class="col-sm-10">     
            <textarea name="creatures" rows="7" class="form-control"></textarea>
        </div>
    </div>
    
    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">Traps</label>
        
        <div class="col-sm-10">     
            <textarea name="traps" rows="7" class="form-control"></textarea>
        </div>
    </div>
    
    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">Treasure</label>
        
        <div class="col-sm-10">     
            <textarea name="treasure" rows="7" class="form-control"></textarea>
        </div>
    </div>
    
    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">Combatants</label>
        
        <div class="col-sm-10">     
            <span class="btn btn-default add-combatant">
                <span class="glyphicon glyphicon-plus-sign"></span>
                Add Combatant
            </span>
        </div>
    </div>
    
    <div id="combatant-super-container"></div>
    
    <input type="submit" value="Create Encounter" class="btn btn-primary" />

</form>