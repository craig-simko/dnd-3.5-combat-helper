
<h2>Areas</h2>

<div class="row">    
    <div class="col-xs-4">
        <a href="index.php?a=create-area">
            <div class="btn btn-primary">
                Create Area
            </div>
        </a>
    </div>

    <div class="col-xs-4">
        <a href="index.php?a=view-areas">
            <div class="btn btn-primary">
                View Areas
            </div>
        </a>
    </div>
</div>

<h2>Encounters</h2>

<div class="row">    
    <div class="col-xs-4">
        <a href="index.php?a=create-encounter">
            <div class="btn btn-primary">
                Create Encounter
            </div>
        </a>
    </div>
    
    <div class="col-xs-4">
        <a href="index.php?a=view-encounters">
            <div class="btn btn-primary">
                View Encounters
            </div>
        </a>
    </div>
</div>

<h2>Combatants</h2>

<div class="row">    
    <div class="col-xs-4">
        <a href="index.php?a=create-combatant">
            <div class="btn btn-primary">
                Create Combatant
            </div>
        </a>
    </div>
    
    <div class="col-xs-4">
        <a href="index.php?a=view-combatants">
            <div class="btn btn-primary">
                View Combatants
            </div>
        </a>
    </div>
</div>