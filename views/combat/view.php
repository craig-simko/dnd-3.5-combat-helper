<?PHP

if(isset($_SESSION['activeCombat'])):

$combatantList = CombatantFactory::GetCombatantList();

?>

<div class="row">
    <div class="col-sm-12">
        <div class="center-block combat-header">
            <img height="50" width="50" src="assets/images/crossed-swords.png" />
            Combat!
            <img height="50" width="50" src="assets/images/crossed-swords.png" />
        </div>
    </div>
</div>

<div class="row combat-action-row">    
    <div class="col-sm-2 col-xs-1">
    </div>

    <div class="col-sm-2 col-xs-3">
        Add Combatant:
    </div>

    <div class="col-sm-3 col-xs-4">
        <select name="" class="combatant-add-select form-control">
            <?php foreach($combatantList as $combatantName): ?>

            <option value="<?= $combatantName ?>"><?= $combatantName ?></option>

            <?php endforeach; ?>
        </select>
    </div>

    <div class="col-sm-1 col-xs-2">
        <div class="inline-container">
            <input type="number" class="combatant-add-amount form-control" step="1" value="1" />
        </div>
    </div>

    <div class="col-sm-1 col-xs-1">
        <span class="add-combatant btn btn-default">
            <span class="glyphicon glyphicon-plus"></span>
        </span>
    </div>

    <div class="col-sm-4 col-xs-1"> 
    </div>
</div>

<div class="row combat-action-row">
    <div class="col-xs-2 right-align">
        Round Timer:
    </div>

    <div class="col-xs-6">
        <input type="text" class="form-control round-timer-details" placeholder="Details about what happens when this timer is up." />
    </div>

    <div class="col-xs-1">
        <input type="number" class="form-control round-timer-round-trigger" step="1" />
    </div>

    <div class="col-xs-2">
        <div class="btn btn-default round-timer-add">
            <span class="glyphicon glyphicon-plus"></span>
            <span class="glyphicon glyphicon-bell"></span>
        </div>
    </div>
</div>

<?php

endif;

?>

<div id="combat-container">
    <?php

        if(isset($_SESSION['activeCombat'])){

            $combat = $_SESSION['activeCombat'];
            $combatants = $combat->CombatantList();

            include_once('views/combat/GetInitiative.php');

        }

    ?>
</div>