
<div class="combat-initiative-container">

    <div class="row">
        <div class="col-sm-12 initiative-header">
            Roll Initiative!
        </div>
    </div>

    <?php
        
    foreach($combatants as $i => $c):
    
        ?>
        
        <div class="row">
            <div class="col-xs-6">
                <div class="right-align combat-initiative-combatant-name">
                    <?php echo $c; ?>
                </div>
            </div>
            
            <div class="col-xs-6 combat-initiative-combatant-name">
                <input type="number" class="form-control init-combatant" step="1" value="" name="init[<?php echo $i; ?>]" data-combatant="<?php echo $i; ?>" />
            </div>
        </div>
        
        <?php
    
    endforeach;

    ?>

    <div class="row">
        <div class="col-sm-12" style="text-align: center;">
            <div class="btn btn-default start-combat" style="padding-top: 5px; padding-bottom: 5px; margin-top: 5px;">
                <img height="15" width="15" src="assets/images/crossed-swords.png" />
                Start Combat!
            </div>
        </div>
    </div>
</div>