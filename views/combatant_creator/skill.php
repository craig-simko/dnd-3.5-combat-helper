<?php 

include_once('../../config.php');

$skillCount = $_POST['skill_count'];

?>

<div class="form-group">        
    <label for="inputDamageResistance" class="col-sm-3 control-label">Skill</label>
    
    <div class="col-sm-9">
        <div class="form-inline">  
                                                   
            <select name="skills[<?= $skillCount ?>][name]" class="form-control skill-select">
                <?php foreach($SKILLS as $skill => $attr): ?>
                
                <option value="<?= $skill ?>" data-attr="<?= $attr ?>"><?= $skill . ' (' . $attr . ')' ?></option>
                
                <?php endforeach; ?>
            </select>
            
            Bonus: <input name="skills[<?= $skillCount ?>][bonus]" type="number" class="form-control skill-bonus" style="max-width: 65px;" value="0" />
            
            + <span class="skill-attr-mod"></span>
            
            = <span class="skill-total"></span>
            
            <div class="btn btn-danger remove-skill"><span class="glyphicon glyphicon-minus"></span>Remove Skill</div>
        </div>
    </div>
    
        
</div>