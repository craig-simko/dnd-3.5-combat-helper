<?php

if(isset($_POST['name'])){
    
    $name                = $_POST['name'];
    
    $flavorText          = $_POST['flavor-text'];
    
    $descriptor          = $_POST['descriptor'];
    
    $sizeCategory        = $_POST['size-category'];
    
    $baseHitPoints       = $_POST['base-hit-points'];
    
    $alignmentLawVsChaos = $_POST['alignmentLawVsChaos'];
    $alignmentGoodVsEvil = $_POST['alignmentGoodVsEvil'];
    $landSpeed           = $_POST['speed-land'];
    $swimSpeed           = $_POST['speed-swim'];
    $flySpeed            = $_POST['speed-fly'];
    $burrowSpeed         = $_POST['speed-burrow'];
    $climbSpeed          = $_POST['speed-climb'];
    $creatureTypes       = $_POST['creatureType'];
    $creatureSubTypes    = $_POST['creatureSubType'];
    $hitDice             = $_POST['hit-dice'];
    
    $languages = array();
    
    if(isset($_POST['languages'])){
        $languages           = $_POST['languages'];
        
        if(!is_array($languages)){
            $languages = array($_POST['languages']);
        }    
    }
    
    $STR = $_POST['STR'] == '' ? null : $_POST['STR'];
    $DEX = $_POST['DEX'] == '' ? null : $_POST['DEX'];
    $CON = $_POST['CON'] == '' ? null : $_POST['CON'];
    $INT = $_POST['INT'] == '' ? null : $_POST['INT'];
    $WIS = $_POST['WIS'] == '' ? null : $_POST['WIS'];
    $CHA = $_POST['CHA'] == '' ? null : $_POST['CHA']; 

    $attributes          = array(
                                'STR' => array('ORIGINAL' => $STR, 'CURRENT' => $STR),
                                'DEX' => array('ORIGINAL' => $DEX, 'CURRENT' => $DEX),
                                'CON' => array('ORIGINAL' => $CON, 'CURRENT' => $CON),
                                'INT' => array('ORIGINAL' => $INT, 'CURRENT' => $INT),
                                'WIS' => array('ORIGINAL' => $WIS, 'CURRENT' => $WIS),
                                'CHA' => array('ORIGINAL' => $CHA, 'CURRENT' => $CHA)
                            );
    
    $saves               = array(
                                array('type' => 'REF',  'base' => $_POST['saveBonusREF'],  'attrs' => $_POST['saveBonusREFAttrs']),
                                array('type' => 'FORT', 'base' => $_POST['saveBonusFORT'], 'attrs' => $_POST['saveBonusFORTAttrs']),
                                array('type' => 'WILL', 'base' => $_POST['saveBonusWILL'], 'attrs' => $_POST['saveBonusWILLAttrs']),
                            );

    $attackModes = array();
    
    if(isset($_POST['attack-mode'])){
        
        foreach($_POST['attack-mode'] as $attackMode){
                       
            $attackModes[$attackMode['attack-mode-designation']] = $attackMode['attacks'];
        }
    }
    
    $armorClass = array(   'armorBonus'        => $_POST['armor-class-armor'],
                           'shieldBonus'       => $_POST['armor-class-shield'],
                           'sizeModifier'      => $_POST['armor-class-size'],
                           'dodgeBonus'        => $_POST['armor-class-dodge'],
                           'deflectionBonus'   => $_POST['armor-class-deflection'],
                           'naturalArmorBonus' => $_POST['armor-class-natural'],
                           'maxDexBonus'       => $_POST['armor-class-max-dex'] == '' ? null : $_POST['armor-class-max-dex'],
                           'attributes'        => $_POST['armor-class-attributes'] );
                                    
    $tactics             = $_POST['tactics'];
    $morale              = $_POST['morale'];
    $hasCombatReflexes   = isset($_POST['has-combat-reflexes']) ? boolval($_POST['has-combat-reflexes']) : false;
    $initiativeBonus     = $_POST['initiative-bonus'];

    $damageReductions    = array();
    $damageResistances   = array();
    $skills              = array();
    $specialAbilities    = array();

    if(isset($_POST['damage-reductions'])){
        foreach($_POST['damage-reductions'] as $dr){
            $damageReductions[] = array('types' => $dr['types'], 'amount' => $dr['amount']);
        }    
    }
    
    if(isset($_POST['damage-reistances'])){
        foreach($_POST['damage-reistances'] as $dr){
            $damageResistances[] = array('types' => $dr['types'], 'amount' => $dr['amount']);
        }    
    }
    
    if(isset($_POST['skills'])){
        foreach($_POST['skills'] as $skill){
            $skills[] = array('skill' => $skill['name'], 'bonus' => $skill['bonus']);
        }
    }
    
    if(isset($_POST['special-abilities'])){
        foreach($_POST['special-abilities'] as $specialAbility){
            
            $specialAbilities[] = array(
                                        'name'        => $specialAbility['name'],
                                        'type'        => $specialAbility['type'],
                                        'description' => $specialAbility['description']
                                    );
        }
    }
        
    $combatGear = $_POST['combat-gear'];
    
    $armorClassAttribute = 'DEX'; //$_POST['armor-class-attribute'];
    $initiativeAttribute = 'DEX'; //$_POST['initiative-attribute'];
    $temporaryHp         = 0;     //$_POST['temporary-hp'];  
        
    $combatant = CombatantFactory::SerializeCombatantData(
                        $name,
                        $flavorText,
                        $baseHitPoints,
                        $sizeCategory,
                        $alignmentLawVsChaos,
                        $alignmentGoodVsEvil,
                        $landSpeed,
                        $swimSpeed,
                        $flySpeed,
                        $burrowSpeed,
                        $climbSpeed,
                        $skills,
                        $creatureTypes,
                        $creatureSubTypes,
                        $hitDice,
                        $attributes,
                        $saves,
                        $attackModes,
                        $armorClass,
                        $tactics,
                        $morale,
                        $combatGear,
                        $specialAbilities,
                        $hasCombatReflexes,
                        $initiativeBonus,
                        $languages,
                        $damageReductions,
                        $damageResistances,
                        $initiativeAttribute,
                        $temporaryHp,
                        $descriptor            
                    );
                    
    $combatantData = file_get_contents('combatants/' . $name . '.json');
                    
    $combatant = CombatantFactory::DeserializeCombatantData($combatantData);
        
    echo '
    <div class="alert alert-success">
        You have successfully created the combatant: <em><strong>' . $name . '</strong></em>
    </div>';
    
}


?>

<div class="combatant-creator-container">
    
    <h1>Combatant Creator</h1>

    <form class="form-horizontal" method="post">
    
        <!-- NAME -->        
        <div class="form-group">        
            <label for="inputDamage" class="col-sm-2 control-label">Name</label>
            
            <div class="col-sm-10">      
                <div class="form-inline">  
                                   
                    <input name="name"         type="text"   class="form-control" id="inputName" placeholder="Goblin A" />
                    Size
                    <select name="size-category" id="size-category" class="form-control">
                        <?php foreach($SIZES as $size_category => $mod): ?>
                            <option value="<?= $size_category ?>" <?= $size_category == 'MEDIUM' ? 'selected="selected"' : '' ?>><?= $size_category ?></option>
                        <?php endforeach; ?>
                    </select>
                    
                    Descriptor
                    <input name="descriptor" type="text" class="form-control" style="width: 40%;" placeholder="Male old human expert 4/rogue 2" />
                </div>
            </div>
        </div>
        
        <div class="form-group">        
            <label for="inputDamage" class="col-sm-2 control-label">Flavor Text</label>
            
            <div class="col-sm-10">     
                <textarea name="flavor-text" class="form-control"></textarea>
            </div>
        </div>
        
        <div class="form-group">        
            <label for="inputDamage" class="col-sm-2 control-label">Combat Gear</label>
            
            <div class="col-sm-10">     
                <textarea name="combat-gear" class="form-control"></textarea>
            </div>
        </div>
        
        <!-- Speed -->
        <div class="form-group">        
            <label for="inputDamage" class="col-sm-2 control-label">Speed</label>
            
            <div class="col-sm-10">      
                <div class="form-inline">
                    Land Speed   <input name="speed-land"   type="number" class="form-control" style="max-width: 65px;" step="5" value="0" />
                    Swim Speed   <input name="speed-swim"   type="number" class="form-control" style="max-width: 65px;" step="5" value="0" />
                    Fly Speed    <input name="speed-fly"    type="number" class="form-control" style="max-width: 65px;" step="5" value="0" />
                    Burrow Speed <input name="speed-burrow" type="number" class="form-control" style="max-width: 65px;" step="5" value="0" />
                    Climb Speed  <input name="speed-climb"  type="number" class="form-control" style="max-width: 65px;" step="5" value="0" />
                </div>
            </div>    
        </div>
        
        <!-- Alignment and Type -->
        <div class="form-group">
            <div class="col-sm-12">
                <div class="form-inline">                    
                    <label for="inputSTR" class="col-sm-2 control-label">Alignment</label>
                    <select name="alignmentLawVsChaos" class="form-control col-sm-1">
                        <?php foreach($ALIGNMENTS_LAW_VS_CHAOS as $alignment): ?>
                        
                            <option value="<?= $alignment ?>" <?= $alignment == 'NEUTRAL' ? 'selected="selected"' : '' ?>><?= $alignment ?></option>
                        
                        <?php endforeach; ?>
                    </select>
                    
                    <select name="alignmentGoodVsEvil" class="form-control col-sm-1">
                        <?php foreach($ALIGNMENTS_GOOD_VS_EVIL as $alignment): ?>
                        
                            <option value="<?= $alignment ?>" <?= $alignment == 'NEUTRAL' ? 'selected="selected"' : '' ?>><?= $alignment ?></option>
                        
                        <?php endforeach; ?>
                    </select>
              
                    <label for="inputSTR" class="col-sm-2 control-label">Creature Type(s)</label>
                    <select multiple="true" name="creatureType[]" class="form-control col-sm-1">
                        <?php foreach($CREATURE_TYPES as $type): ?>
                        
                            <option value="<?= $type ?>" <?= $type == 'HUMANOID' ? 'selected="selected"' : '' ?>><?= $type ?></option>
                        
                        <?php endforeach; ?>
                    </select>
                    
                    <select multiple="true" name="creatureSubType[]" class="form-control col-sm-1">
                        <?php foreach($CREATURE_SUBTYPES as $type): ?>
                        
                            <option value="<?= $type ?>" <?= $type == 'NONE' ? 'selected="selected"' : '' ?>>
                                <?= $type ?>
                            </option>
                        
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        
        <!-- MAX HIT POINTS -->
        <div class="form-group">
            <div class="col-sm-12">
                <div class="form-inline">                    
                    <label for="inputSTR" class="col-sm-2 control-label">Base Hit Points</label>
                    <input type="number" id="base-hit-points-input" class="form-control col-sm-3" name="base-hit-points" value="1" id="inputMaxHitPoints" placeholder="0" />
              
                    <label for="inputDEX" class="col-sm-1 control-label">Hit Dice</label>
                    <input type="number" id="hit-dice-input" class="form-control col-sm-3" name="hit-dice" value="1" />
                    
                    <label for="hit-point-container" class="col-sm-2 control-label"> = Hit Points</label>
                    <span id="hit-point-container" class="col-sm-2" style="font-size: 16px; font-weight: bold;">1</span>
                </div>
            </div>
        </div>
        
        <!-- Physical Attributes -->
        <div class="form-group">
            <div class="col-sm-12">
                <div class="form-inline">                    
                    <label for="inputSTR" class="col-sm-2 control-label">STR</label>
                    <input type="number" class="col-sm-2 form-control attr-input" id="str-input" name="STR" placeholder="10" value="10" />
              
                    <label for="inputDEX" class="col-sm-1 control-label">DEX</label>
                    <input type="number" class="col-sm-2 form-control attr-input" id="dex-input" name="DEX" placeholder="10" value="10" />
              
                    <label for="inputCON" class="col-sm-1 control-label">CON</label>
                    <input type="number" class="col-sm-2 form-control attr-input" id="con-input" name="CON" placeholder="10" value="10" />
                </div>
            </div>
        </div>
        
        <!-- Mental Attributes -->
        <div class="form-group">
            <div class="col-sm-12">
                <div class="form-inline">                    
                    <label for="inputINT" class="col-sm-2 control-label">INT</label>
                    <input type="number" class="col-sm-2 form-control attr-input" id="int-input" name="INT" placeholder="10" value="10" />
              
                    <label for="inputWIS" class="col-sm-1 control-label">WIS</label>
                    <input type="number" class="col-sm-2 form-control attr-input" id="wis-input" name="WIS" placeholder="10" value="10" />
              
                    <label for="inputCHA" class="col-sm-1 control-label">CHA</label>
                    <input type="number" class="col-sm-2 form-control attr-input" id="cha-input" name="CHA" placeholder="10" value="10" />
                </div>
            </div>
        </div>
        
        <!-- SAVES -->
        
        <!-- FORT -->
        <div class="form-group">
            <div class="col-sm-12">
                <div class="form-inline">                    
                    <label for="saveBonusFORT" class="col-sm-2 control-label">Fort Save Bonus</label>
                    <input type="number" class="col-sm-2 form-control" name="saveBonusFORT" id="saveBonusFortInput" placeholder="0" value="0" />
                    
                    <label for="saveBonusFORTAttrs" class="col-sm-2 control-label">Fort Save Attr(s)</label>
                    <select multiple="true" name="saveBonusFORTAttrs[]" class="form-control col-sm-1">
                        <?php foreach($ATTRIBUTES as $attr): ?>
                        
                            <option value="<?= $attr ?>" <?= $attr == 'CON' ? 'selected="selected"' : '' ?>>
                                <?= $attr ?>
                            </option>
                        
                        <?php endforeach; ?>
                    </select>
                    
                    <label for="saveFortTotal" class="col-sm-1 control-label">=</label>
                    <span id="saveFortTotal" style="font-size: 16px; font-weight: bold;">0</span>
                </div>
            </div>
        </div>
                
        <!-- REF -->
        <div class="form-group">
            <div class="col-sm-12">
                <div class="form-inline">                    
                    <label for="saveBonusREF" class="col-sm-2 control-label">Reflex Save Bonus</label>
                    <input type="number" class="col-sm-2 form-control" name="saveBonusREF" id="saveBonusRefInput" placeholder="0" value="0" />
                    
                    <label for="saveBonusREFAttrs" class="col-sm-2 control-label">Reflex Save Attr(s)</label>
                    <select multiple="true" name="saveBonusREFAttrs[]" class="form-control col-sm-1">
                        <?php foreach($ATTRIBUTES as $attr): ?>
                        
                            <option value="<?= $attr ?>" <?= $attr == 'DEX' ? 'selected="selected"' : '' ?>>
                                <?= $attr ?>
                            </option>
                        
                        <?php endforeach; ?>
                    </select>
                    
                    <label for="saveRefTotal" class="col-sm-1 control-label">=</label>
                    <span id="saveRefTotal" style="font-size: 16px; font-weight: bold;">0</span>
                </div>
            </div>
        </div>
        
        <!-- WILL -->
        <div class="form-group">
            <div class="col-sm-12">
                <div class="form-inline">                    
                    <label for="saveBonusWILL" class="col-sm-2 control-label">Will Save Bonus</label>
                    <input type="number" class="col-sm-2 form-control" name="saveBonusWILL" id="saveBonusWillInput" placeholder="0" value="0" />
                    
                    <label for="saveBonusWILLAttrs" class="col-sm-2 control-label">Will Save Attr(s)</label>
                    <select multiple="true" name="saveBonusWILLAttrs[]" class="form-control col-sm-1">
                        <?php foreach($ATTRIBUTES as $attr): ?>
                        
                            <option value="<?= $attr ?>" <?= $attr == 'WIS' ? 'selected="selected"' : '' ?>>
                                <?= $attr ?>
                            </option>
                        
                        <?php endforeach; ?>
                    </select>
                    
                    <label for="saveWillTotal" class="col-sm-1 control-label">=</label>
                    <span id="saveWillTotal" style="font-size: 16px; font-weight: bold;">0</span>
                </div>
            </div>
        </div>
        
        <!-- Attack Modes -->
        <hr />
        
        <h3>Attack Modes 
            <span class="add-attack-mode btn btn-default"><span class="glyphicon glyphicon-plus"></span>Add Attack Mode</span>
        </h3>
                
        <div id="attack-mode-super-container"></div>
        
        
        <hr />
        
        <div class="form-group">        
            <label for="inputDamage" class="col-sm-2 control-label">Armor Class Bonuses</label>
            
            <div class="col-sm-10">      
                <div class="form-inline">  
                                                           
                    Armor <input name="armor-class-armor"           type="number" class="form-control" style="max-width: 65px;" value="0" />
                    Shield <input name="armor-class-shield"         type="number" class="form-control" style="max-width: 65px;" value="0" />
                    Size <input name="armor-class-size" id="armor-class-size"            type="number" class="form-control" style="max-width: 65px;" value="0" />
                    Dodge <input name="armor-class-dodge"           type="number" class="form-control" style="max-width: 65px;" value="0" />
                    Deflection <input name="armor-class-deflection" type="number" class="form-control" style="max-width: 65px;" value="0" />
                    Natural <input name="armor-class-natural"       type="number" class="form-control" style="max-width: 65px;" value="0" />
                    Max Dex <input name="armor-class-max-dex"       type="number" class="form-control" style="max-width: 65px;" value="0" />
                    Attribute(s) 
                    <select name="armor-class-attributes" class="form-control">
                        <?php foreach($ATTRIBUTES as $attr): ?>
                        
                        <option value="<?= $attr ?>" <?= $attr == 'DEX' ? 'selected="selected"' : '' ?>><?= $attr ?></option>
                        
                        <?php endforeach; ?>
                    </select>
                    
                </div>
            </div>    
        </div>
        
        <hr />
        
        <!-- Tactics -->
        <div class="form-group">
            <label for="inputTactics" class="col-sm-2 control-label">Tactics</label>
            <div class="col-sm-10">
                <textarea name="tactics" class="form-control" placeholder="What the combatant will do and when."></textarea>
            </div>
        </div>
        
        <!-- Morale -->
        <div class="form-group">
            <label for="inputMorale" class="col-sm-2 control-label">Morale</label>
            <div class="col-sm-10">
                <textarea name="morale" class="form-control" placeholder="How long will this combatant fight on? When will they retreat?"></textarea>
            </div>
        </div>
                
        <div class="form-group">        
            <label for="inputDamage" class="col-sm-3 control-label">Has Combat Reflexes?</label>
            
            <div class="col-sm-9">      
                <div class="form-inline">  
                                                           
                    <input name="has-combat-reflexes" type="checkbox" class="form-control" style="max-width: 65px;" value="0" />
                    Initiative Bonus <input name="initiative-bonus" type="number" class="form-control" style="max-width: 65px;" value="0" />
                                       
                    Languages
                    <select name="languages[]" class="form-control" multiple="multiple">
                        <?php foreach($LANGUAGES as $language): ?>
                        
                        <option value="<?= $language ?>"><?= $language ?></option>
                        
                        <?php endforeach; ?>
                    </select>
                    
                </div>
            </div>    
        </div>
        
        <hr />        
        
        <h3>
            Damage Reduction
            
            <div class="btn btn-default add-damage-reduction">
                <span class="glyphicon glyphicon-plus"></span>
                Add Damage Reduction
            </div>
        </h3>
            
        <div class="damage-reduction-super-container">
        
        </div>
        
        <h3>
            Damage Resistance
            
            <div class="btn btn-default add-damage-resistance">
                <span class="glyphicon glyphicon-plus"></span>
                Add Damage Resistance
            </div>
        </h3>
            
        <div class="damage-resistance-super-container">
        
        </div>
        
        <h3>
            Special Abilities
            
            <div class="btn btn-default add-special-ability">
                <span class="glyphicon glyphicon-plus"></span>
                Add Special Ability
            </div>
        </h3>
            
        <div class="special-ability-super-container">
        
        </div>
        
        <h3>
            Skills
            
            <div class="btn btn-default add-skill">
                <span class="glyphicon glyphicon-plus"></span>
                Add Skill
            </div>
        </h3>
            
        <div class="skill-super-container">
        
        </div>
        
        <input type="submit" class="btn btn-info" value="Create Combatant" />
    </form>

</div>