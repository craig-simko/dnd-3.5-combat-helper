<?php 

include_once('../../config.php');

$attackModeCount = $_POST['attack_mode_count'];
$attackCount     = $_POST['attack_count'];

?>

<div class="remove-attack btn btn-danger"><span class="glyphicon glyphicon-minus"></span>Remove Attack</div>

<div class="form-group">
    <div class="col-sm-12">
        <div class="form-inline">
      
            <label for="attack-mode-type[]" class="col-sm-2 control-label">Type</label>
            <select name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][type]" class="form-control col-sm-4">
                <option value="melee">
                    Melee
                </option>
                
                <option value="melee-touch">
                    Melee Touch
                </option>
                
                <option value="ranged">
                    Ranged
                </option>
                
                <option value="ranged-touch">
                    Ranged Touch
                </option>
            </select>
      
            <label for="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][range]" class="col-sm-1 control-label">Range</label>
            <input name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][range]" type="number" style="max-width: 65px;" class="col-sm-1 form-control" placeholder="5" value="5" step="5"/>
                    
            <label for="attack-mode-crit-range[]" class="col-sm-1 control-label">Crit Range</label>
            <select name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][crit_range]" style="max-width: 65px;" class="col-sm-1 form-control">
                <?php for($c = 1; $c <= 20; $c++): ?>
                
                <option value="<?= $c ?>" <?= $c == 20 ? 'selected="selected"' : '' ?>>
                    <?= $c ?>
                </option>
                
                <?php endfor; ?>
            </select>-20
                        
            
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <div class="form-inline attack-bonus-container">
            <label for="attack-mode-bonus[]" class="col-sm-2 control-label">Base Attack Bonus</label>
            <input name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][base_attack_bonus]" type="number" style="max-width: 65px;" class="col-sm-1 form-control base-attack-bonus" placeholder="1" value="0" step="1"/>
            
            <label for="attack-mode-bonus[]" class="col-sm-1 control-label">Extra Bonus</label>
            <input name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][attack_bonus]" type="number" style="max-width: 65px;" class="col-sm-1 form-control attack-mode-bonus" placeholder="1" value="0" step="1"/>
            
            <label class="col-sm-1 control-label">Attr</label>
            <select multiple="true" name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][attack_attrs][]" class="form-control col-sm-1 attack-attrs">
                <?php foreach($ATTRIBUTES as $attr): ?>
                
                <option value="<?= $attr ?>" <?= $attr == 'STR' ? 'selected="selected"' : '' ?>>
                    <?= $attr ?>
                </option>
                
                <?php endforeach; ?>
            </select>
            <span class="attack-attr-mod col-sm-1"></span>
            
            <label class="col-sm-1 control-label"> Size Mod </label>
            <span class="attack-bonus-size-mod col-sm-1"></span>

            <label class="col-sm-1 control-label"> = </label>
            <span class="attack-bonus-total col-sm-1"></span>
        </div>
    </div>
</div>

<div class="add-damage btn btn-success" data-attack-count="<?= $attackCount ?>"><span class="glyphicon glyphicon-plus"></span>Add Damage</div>
        
<div class="damage-super-container"></div>