<?php 

include_once('../../config.php');

$damageReductionCount = $_POST['damage_reduction_count'];

?>

<div class="form-group">        
    <label for="inputDamageReduction" class="col-sm-3 control-label">Damage Reduction Types</label>
    
    <div class="col-sm-9">
        <div class="form-inline">  
                                                   
            <select name="damage-reductions[<?= $damageReductionCount ?>][types]" class="form-control" multiple="multiple">
                <?php foreach($DAMAGE_TYPES as $type): ?>
                
                <option value="<?= $type ?>"><?= $type ?></option>
                
                <?php endforeach; ?>
            </select>
            
            Amount: <input name="damage-reductions[<?= $damageReductionCount ?>][amount]" type="number" class="form-control" style="max-width: 65px;" value="0" />
            
            <div class="btn btn-danger remove-damage-reduction"><span class="glyphicon glyphicon-minus"></span>Remove Damage Reduction</div>
        </div>
    </div>
    
        
</div>