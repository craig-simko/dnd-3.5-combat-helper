<?php 

include_once('../../config.php');

$specialAbilityCount = $_POST['special_ability_count'];

?>

<div class="form-group">        
    <label for="inputDamageResistance" class="col-sm-3 control-label">Special Ability Name</label>
    
    <div class="col-sm-3">
        <input name="special-abilities[<?= $specialAbilityCount ?>][name]" class="form-control" />
    </div>

    <div class="col-sm-1">
        <strong>Type:</strong>
    </div>
    
    <div class="col-sm-3">
        
        <select name="special-abilities[<?= $specialAbilityCount ?>][type]" class="form-control">
            <?php foreach($SPECIAL_ABILITY_TYPES as $key => $type): ?>
            
            <option value="<?= $key ?>"><?= $type ?></option>
            
            <?php endforeach; ?>
        </select>
    </div>
        
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Description:</label>

    <div class="col-sm-7">                
        <textarea name="special-abilities[<?= $specialAbilityCount ?>][description]" class="form-control col-sm-12"></textarea>
    </div>
    
    <div class="col-sm-2">
        <div class="btn btn-danger remove-special-ability"><span class="glyphicon glyphicon-minus"></span>Remove</div>
    </div>
</div>