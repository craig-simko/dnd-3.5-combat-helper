<?php

include_once('../../config.php');

$attackModeCount = $_POST['attack_mode_count'];
$countDisplay    = $_POST['count_display'];

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            Attack Mode #<span class="attack-mode-count"><?= $countDisplay ?></span>

            <div class="pull-right remove-attack-mode">
                <span class="btn btn-danger"><span class="glyphicon glyphicon-minus"></span>Remove Attack Mode</span>
            </div>
        </h3>
    </div>
    <div class="panel-body">
        <div class="form-group">        
            <label for="inuptAttackModeName" class="col-sm-3 control-label">Attack Mode Designation</label>

            <div class="col-sm-6">        
                <input type="text" class="form-control" name="attack-mode[<?= $attackModeCount ?>][attack-mode-designation]" placeholder="Full Attack / Standard Attack / Full Attack with Sneak"/>
            </div>

            <div class="col-sm-3">
                <div class="add-attack btn btn-success"><span class="glyphicon glyphicon-plus"></span>Add Attack</div>
            </div>    
        </div>
        
        <div class="attack-super-container" data-attack-mode-count="<?= $attackModeCount ?>"></div>
                
    </div>
</div>