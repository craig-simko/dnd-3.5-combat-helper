<?php

include_once('../../config.php');

$attackModeCount = $_POST['attack_mode_count'];
$attackCount     = $_POST['attack_count'];
$damageCount     = $_POST['damage_count'];

?>

<div class="form-group">        
    <label for="inputDamage" class="col-xs-2 control-label">Damage</label>
    
    <div class="col-xs-10">      
        <div class="form-inline">  
            <div class="col-sm-2">
                <input name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][damages][<?= $damageCount ?>][damage_die_count]" type="number" placeholder="1" class="form-control damage-die-count damage-input" value="0" style="max-width: 65px;" />
                <span>d</span>
            </div>
                        
            <div class="col-sm-2">
                <span>
                    <select name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][damages][<?= $damageCount ?>][damage_die_type]" class="form-control damage-die-type damage-input" style="max-width: 65px;">
                        <option value="0">0</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="6">6</option>
                        <option value="8">8</option>
                        <option value="10">10</option>
                        <option value="12">12</option>
                    </select>

                    <span>+</span>
                </span>

            </div>

            <!-- <div class="col-sm-1">+</div> -->

            <div class="col-sm-2">                
                <span>
                    <input type="number" name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][damages][<?= $damageCount ?>][damage_bonus]" class="form-control col-sm-1 damage-bonus damage-input" value="0" style="max-width: 65px;" />
                </span>
            </div>
            
            <label class="col-sm-1 control-label">Attr</label>
            <select multiple="true" name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][damages][<?= $damageCount ?>][damage_attrs][]" id="melee-damage-attrs" class="form-control col-sm-1 damage-attrs damage-input">
                <option value="NONE">None</option>
                <?php foreach($ATTRIBUTES as $attr): ?>
                
                <option value="<?= $attr ?>" <?= $attr == 'STR' ? 'selected="selected"' : '' ?>>
                    <?= $attr ?>
                </option>
                
                <?php endforeach; ?>
            </select>
            <span class="damage-attr-mod col-sm-1"></span>
            
            Attr Mod Multiplier: 
            <select name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][damages][<?= $damageCount ?>][damage_attr_multiplier]" class="form-control damage-attr-multiplier damage-input">
                <option value="0.5">0.5</option>               
                <option value="1" selected="selected">1</option>
                <option value="1.5">1.5</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>  
    
</div>

<div class="form-group">        
    <label class="col-xs-2 control-label">Type(s)</label>
    <div class="col-xs-10">      
        <div class="form-inline">
            <select name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][damages][<?= $damageCount ?>][damage_types][]" class="form-control col-sm-1 damage-input" multiple="multiple">
                <?php foreach($DAMAGE_TYPES as $type): ?>
                
                <option value="<?= $type ?>"><?= $type ?></option>
                
                <?php endforeach; ?>
            </select>
            
            Crit Multiplier: <input type="number" class="form-control damage-input" name="attack-mode[<?= $attackModeCount ?>][attacks][<?= $attackCount ?>][damages][<?= $damageCount ?>][crit_multiplier]" value="1" style="max-width: 65px;" />
            <span title="Leave at 1 for damage that isn't multiplied on a crit. Such as sneak attack damage.">[?]</span>
            
            <span class="damage-total"></span>
            
            <div class="btn btn-danger remove-damage">
                <span class="glyphicon glyphicon-minus-sign"></span>
                Remove
            </div>
        </div>
    </div>
</div>

<hr />