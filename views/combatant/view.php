<?php

$skills    = $combatant->Skills();

?>

<div class="panel panel-default panel-combatant">

    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="combatant-name-full-size">

                    <?php

                        $names = explode(' ', $combatant->Name());

                        foreach($names as $name):

                            $name   = strtoupper($name);
                            $length = strlen($name);
                            $parts  = array();
                            $num    = 1;

                            $parts[0] = substr($name, 0, $num);
                            $parts[1] = substr($name, $num, $length );

                            echo '
                            <span class="combatant-name-first-letter-full-size">' . $parts[0] . '</span>
                            <span class="combatant-name-remainder-full-size">' . $parts[1] . '</span>';

                        endforeach;

                        if(file_exists(ROOT . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'combatants' . DIRECTORY_SEPARATOR . '' . $combatant->NameTrue() . '.png')):
                            ?>
                                <a class="btn btn-default" target="_blank" href="assets/images/combatants/<?php echo $combatant->NameTrue(); ?>.png">
                                    <img src="assets/images/view-combatant-pic.png" />
                                </a>
                            <?php
                        endif;

                    ?>
                </div>
            </div>

            <div class="col-sm-3">
                
                <span class="combatant-label">
                    Status:
                </span>
                
                <?php if($combatant->IsDisabled()): ?>
                
                <img src="assets/images/status-icons/disabled.png" title="Disabled (0 hp)" />
                
                <?php endif; ?>
                
                <?php if($combatant->IsDead()): ?>
                
                <img src="assets/images/status-icons/dead.png" title="Dead" />
                
                <?php endif; ?>
                
                <?php if($combatant->IsDying()): ?>
                
                <img src="assets/images/status-icons/dying.png" title="Dying (need to stabilize)" />
                
                <?php endif; ?>
                
                <?php if($combatant->IsStable()): ?>
                
                <img src="assets/images/status-icons/stable.png" title="Stable" />
                
                <?php endif; ?>
                
                <?php if($combatant->IsFlatfooted()): ?>
                
                <img src="assets/images/status-icons/flat-footed.png" title="Flat-Footed" />
                
                <?php endif; ?>
            </div>
            
            <div class="col-md-6">
            
                
            
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $combatant->Descriptor() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $combatant->Alignment() . ' ' . $combatant->Size() . ' ' . $combatant->CreatureType() ?>
            </div>
        </div>
        
        
        <div class="row">
            
            <div class="col-md-6">
                
                <div class="row">
                    <div class="col-sm-12">
                        <span class="combatant-label">
                            Init
                        </span>
                        <?= $combatant->InitBonus() ?>;
                        <span class="combatant-label">
                            Senses
                        </span>
                        
                        Listen <?= $skills['Listen']['total_display'] ?>, Spot <?= $skills['Spot']['total_display'] ?>
                    </div>
                </div>
                
                <div class="row combatant-heading-container">
                    <div class="col-sm-12 combatant-heading">DEFENSE</div>
                </div>
                
                <div class="row">
                    <div class="col-sm-1">
                        <span class="combatant-label">
                            AC
                        </span>
                    </div>
                    
                    <div class="col-sm-11">
                        <?= $combatant->ArmorClass('MELEE') ?>, touch <?= $combatant->ArmorClass('MELEE_TOUCH') ?>, flat-footed <?= $combatant->ArmorClass('FLAT_FOOTED') ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-1">
                        <span class="combatant-label">
                            hp
                        </span>
                    </div>
                    
                    <div class="col-sm-11">
                        <?= $combatant->HitPointsString() ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                    
                        <?php
                        
                        echo $combatant->SaveBonusesString();
                        
                        ?>
                                          
                    </div>
                </div>
                
                <div class="row combatant-heading-container">
                    <div class="col-sm-12 combatant-heading">OFFENSE</div>
                </div>
                
                <div class="row">
                    <div class="col-sm-1">
                        <span class="combatant-label">
                            Spd
                        </span>
                    </div>
                    
                    <div class="col-sm-11">
                        <?= $combatant->SpeedString() ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $combatant->GetAttackModesString(); ?>
                    </div>
                </div>
                
                <div class="row combatant-heading-container">
                    <div class="col-sm-12 combatant-heading">TACTICS</div>
                </div>
                
                <div class="row">
                    <div class="col-sm-2">
                        <span class="combatant-label">
                            During Combat
                        </span>
                    </div>
                    
                    <div class="col-sm-10">
                        <?php echo $combatant->Tactics(); ?>
                    </div>
                </div>
                    
                <div class="row">
                    <div class="col-sm-2">
                        <span class="combatant-label">
                            Morale
                        </span>
                    </div>
                    
                    <div class="col-sm-10">
                        <?php echo $combatant->Morale(); ?>
                    </div>
                </div>

            </div>

            <div class="col-md-6">

                <div class="row">&nbsp;</div>    

                <div class="row combatant-heading-container">
                    <div class="col-sm-12 combatant-heading">STATISTICS</div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <span class="combatant-label">Str</span> <?php echo $combatant->AttributeScore('STR'); ?>,
                        <span class="combatant-label">Dex</span> <?php echo $combatant->AttributeScore('DEX'); ?>,
                        <span class="combatant-label">Con</span> <?php echo $combatant->AttributeScore('CON'); ?>,
                        <span class="combatant-label">Int</span> <?php echo $combatant->AttributeScore('INT'); ?>,
                        <span class="combatant-label">Wis</span> <?php echo $combatant->AttributeScore('WIS'); ?>,
                        <span class="combatant-label">Cha</span> <?php echo $combatant->AttributeScore('CHA'); ?>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <span class="combatant-label">Base Atk</span> <?php echo $combatant->BaseAttackBonusString(); ?>;

                        <span class="combatant-label">Grp</span> <?php echo $combatant->GrappleBonusString(); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <span class="combatant-label">Feats</span> <!-- Agile, Skill Focus (Sleight of Hand) -->
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <span class="combatant-label">Skills</span>
                        <?php
                        
                        $skillDisplay = array();
                        
                        foreach($skills as $skill):
                            
                            if($skill['name'] == 'Listen' || $skill['name'] == 'Spot'){
                                continue;
                            }
                            
                            $skillDisplay[] = $skill['name'] . ' ' . $skill['total_display'];
                        
                        endforeach;
                        
                        echo implode(', ', $skillDisplay);
                        ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <span class="combatant-label">Languages</span>
                        <?= implode (', ', $combatant->Languages()) ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <span class="combatant-label">Gear</span>
                        <?= $combatant->Gear() ?>
                    </div>
                </div>

                <div class="row combatant-heading-container">
                    <div class="col-sm-12 combatant-heading">SPECIAL ABILITIES</div>
                </div>
                
                <?php foreach($combatant->SpecialAbilities() as $sa): ?>

                <div class="row">
                    <div class="col-sm-12">

                        <div class="special-ability-body">
                            <span class="combatant-label"><?= $sa['name'] . ' (' . $sa['type'] . ')' ?></span>
                            <?= $sa['description'] ?>
                        </div>

                    </div>
                </div>

                <?php endforeach; ?>

            </div>

        </div>

    </div>

</div>