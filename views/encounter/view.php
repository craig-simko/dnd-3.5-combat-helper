<?php

$encounter      = $_SESSION['activeEncounter'];

function highLight($text, $title = ''){

    $matches      = array();
    $replacements = array();

    $matchesChecks      = array();
    $replacementsChecks = array();

    $matchesSaves      = array();
    $replacementsSaves = array();

    $matchesTreasure      = array();
    $replacementsTreasure = array();

    preg_match_all('/"([^"]+)"/',              $text, $matches);
    preg_match_all("/DC.*check/U",             $text, $matchesChecks);
    preg_match_all("/DC.*save/iU",             $text, $matchesSaves);
    preg_match_all("/worth .*[c|s|g|p|e]p/iU", $text, $matchesTreasure);

    $matches         = $matches[0];
    $matchesChecks   = $matchesChecks[0];    
    $matchesSaves    = $matchesSaves[0];
    $matchesTreasure = $matchesTreasure[0];

    foreach($matches as $match){
        $replacements[] = '<span class="highlight">' . $match . '</span>';
    }

    foreach($matchesChecks as $matchCheck){
        $replacementsChecks[] = '<span class="highlight-check">' . $matchCheck . '</span>';
    }
    
    foreach($matchesSaves as $matchSave){
        $replacementsSaves[] = '<span class="highlight-save">' . $matchSave . '</span>';
    }
    
    foreach($matchesTreasure as $matchTreasure){
        $replacementsTreasure[] = '<span class="highlight-treasure">' . $matchTreasure . '</span>';
    }
    
    $matches      = array_merge($matches, $matchesChecks, $matchesSaves, $matchesTreasure);
    $replacements = array_merge($replacements, $replacementsChecks, $replacementsSaves, $replacementsTreasure);
    
    $ret = ($title == '' ? '' : '<span class="encounter-text-block-title">' . $title . '</span>') . str_replace($matches, $replacements, $text);
 
    return $ret;   
}

$encounterText = array();

if(strlen($encounter->DmText()) > 0){
    $encounterText[] = highLight($encounter->DmText(), '');
}

if(strlen($encounter->Creatures()) > 0){    
    $encounterText[] = highLight($encounter->Creatures(), 'Creature(s):');
}

if(strlen($encounter->Traps()) > 0){
    $encounterText[] = highLight($encounter->Traps(), 'Trap(s):');
}

if(strlen($encounter->Treasure()) > 0){
    $encounterText[] = highLight($encounter->Treasure(), 'Treasure:');
}

$encounterText = implode('<br />', $encounterText);

?>

<div class="encounter-container">
    <div class="row">
        <div class="col-md-12 encounter-title">
            <?= $encounter->EncounterName() ?>        
        </div>
    </div>
        
    <div class="row">
        <div class="col-md-12 encounter-box-text">
            <?= $encounter->BoxText() ?>
        </div>
    </div>
               
    <div class="row">
        <div class="twocolumns encounter-dm-text">
            <?php echo $encounterText; ?>
        </div>
    </div>
    
</div>

<?php

include_once('views/combat/view.php');

?>