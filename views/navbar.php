<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-dnd-combat-helper" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">D&amp;D Helper</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse-dnd-combat-helper">
        
        <ul class="nav navbar-nav">
            <li style="padding-top: 7px; padding-right: 5px;">
                <div style="display: inline-block;">
                    Active Area
                </div>
                
                <div style="display:  inline-block;">
                    <select id="active-area-selector" class="form-control">
                        
                        <option value="">Select...</option>
                    
                        <?php foreach($areaNamesNavBar as $a): ?>
                            
                            <option value="<?= $a ?>" <?= isset($selectedArea) && $selectedArea == $a ? 'selected="selected"' : '' ?>><?= $a ?></option>
                            
                        <?php endforeach; ?>
                    </select>
                </div>
            </li>
        
            <li style="padding-top: 7px; padding-right: 5px;">
                <div style="display: inline-block;">
                    Active Encounter
                </div>
                
                <div style="display:  inline-block;">
                    <select id="active-encounter-selector" class="form-control">
                        
                        <option value="">Select...</option>
                    
                        <?php foreach($encounterNamesNavBar as $e): ?>
                            
                            <option value="<?= $e ?>" <?= isset($selectedEncounter) && $selectedEncounter == $e ? 'selected="selected"' : '' ?>><?= $e ?></option>
                            
                        <?php endforeach; ?>
                    </select>
                </div>
            </li>
        </ul>
        
  </div><!-- /.container-fluid -->
</nav>