<?php 

include_once('../../config.php');

$sidebarCount = $_POST['sidebar_count'];

?>

<div class="form-group">        
    <label for="" class="col-sm-3 control-label">Sidebar Title</label>
    
    <div class="col-sm-9">
        <input type="text" name="sidebars[<?= $sidebarCount ?>][title]" class="form-control" />
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Description</label>
    
    <div class="col-sm-7">
        <textarea name="sidebars[<?= $sidebarCount ?>][description]" class="form-control"></textarea>
    </div>
    
    <div class="col-sm-2">
        <div class="btn btn-danger remove-sidebar"><span class="glyphicon glyphicon-minus"></span>Remove Sidebar</div>
    </div>
</div>

