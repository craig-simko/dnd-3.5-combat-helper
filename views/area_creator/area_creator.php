<?php

$encounters = EncounterFactory::GetEncounterList();

if(isset($_POST['area-name'])){

    $areaName   = $_POST['area-name'];
    $dmText     = $_POST['dm-text'];
    $encounters = $_POST['encounters'];
    $sidebars   = isset($_POST['sidebars']) ? $_POST['sidebars'] : array();      
    
    AreaFactory::SerializeAreaData($areaName, $dmText, $encounters, $sidebars);
    
    ?>  
        <div class="alert alert-success">
            You have successfully created the area: <strong><em><?php echo $areaName; ?></em></strong>
        </div>
    <?php
}

?>

<h1>Area Creator</h1>

<form class="form-horizontal" method="post">

    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">Area Name</label>
        
        <div class="col-sm-10">     
            <input name="area-name" class="form-control" />
        </div>
    </div>
    
    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">DM Text</label>
        
        <div class="col-sm-10">     
            <textarea name="dm-text" rows="7" class="form-control"></textarea>
        </div>
    </div>
    
    <div class="form-group">        
        <label for="" class="col-sm-2 control-label">Encounters</label>
        
        <div class="col-sm-10">     
            <select name="encounters[]" multiple="true" class="form-control">                
                <?php foreach($encounters as $e): ?>
                    <option value="<?php echo $e; ?>"><?php echo $e; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label for="" class="col-sm-2 control-label">Sidebars</label>

        <div class="col-sm-10">     
            <span class="btn btn-default add-area-sidebar">
                <span class="glyphicon glyphicon-plus-sign"></span>
                Add Sidebar
            </span>
        </div>
    </div>

    <div id="sidebar-super-container"></div>

    <input type="submit" value="Create Area" class="btn btn-primary" />

</form>