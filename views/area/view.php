<?php

$area = $_SESSION['activeArea'];

$sidebars = $area->SideBars();

?>

<div class="row">
    <div class="col-md-<?= count($sidebars) > 0 ? '8' : '12' ?>">

        <?php

        $areaImageName = 'assets' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'areas' . DIRECTORY_SEPARATOR . $area->AreaName() . '.png';

        if(file_exists($areaImageName)):
        ?>

        <img src="<?= $areaImageName ?>" class="area-image" />
        
        <?php
        endif;
        
        ?>
    
        <div class="panel panel-parchment">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $area->AreaName() ?></h3>
            </div>
            <div class="panel-body">
                <?= $area->DmText() ?>
            </div>
        </div>
        
    </div>
    
    <?php
    if(count($sidebars) > 0):
    ?>
        <div class="col-md-4">
            
            <h3>Sidebars</h3>
            
            <?php foreach($sidebars as $sidebar): ?>
    
                <div class="panel panel-parchment">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $sidebar['title'] ?></h3>
                    </div>
                    <div class="panel-body">
                        <?= $sidebar['description'] ?>
                    </div>
                </div>
    
            <?php endforeach; ?>
            
        </div>
    <?php
    endif;
    ?>
    
</div>